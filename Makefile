.PHONY: openapi

check-env:
ifndef GOPATH
	$(error GOPATH is undefined)
endif

openapi: check-env
	${GOPATH}/bin/oapi-codegen -generate server -package api pkg/nsmtpd/api/openapi.yaml > pkg/nsmtpd/api/openapi.go
