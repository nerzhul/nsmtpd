group "default" {
    targets = ["nsmtpd", "nimapd"]
}

variable "VERSION" {
    default = "latest"
}

variable "PLATFORMS" {
    default = ["linux/amd64"]
}

target "nsmtpd" {
    dockerfile = "./Dockerfile.nsmtpd"
    platforms = PLATFORMS
    tags = ["nerzhul/nsmtpd:${VERSION}"]
}

target "nimapd" {
    dockerfile = "./Dockerfile.nimapd"
    platforms = PLATFORMS
    tags = ["nerzhul/nimapd:${VERSION}"]
}
