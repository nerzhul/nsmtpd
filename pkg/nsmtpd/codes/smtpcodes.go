package codes

import "github.com/emersion/go-smtp"

// Temporary failures
var ErrTempFailUserDelivery = &smtp.SMTPError{
	Code:         450,
	EnhancedCode: smtp.NoEnhancedCode,
	Message:      "Temporary failure during storage delivery",
}

var ErrTempFailUserLookup = &smtp.SMTPError{
	Code:         451,
	EnhancedCode: smtp.NoEnhancedCode,
	Message:      "Temporary failure during user lookup",
}

var ErrTempFailPermCheck = &smtp.SMTPError{
	Code:         452,
	EnhancedCode: smtp.NoEnhancedCode,
	Message:      "Temporary failure during request validation",
}

// Permanent failures
var ErrPermFailMalformedMailAddr = &smtp.SMTPError{
	Code:         510,
	EnhancedCode: smtp.NoEnhancedCode,
	Message:      "Malformed mail address",
}

var ErrPermFailNoMX = &smtp.SMTPError{
	Code:         510,
	EnhancedCode: smtp.NoEnhancedCode,
	Message:      "No SMTP servers bound to this address",
}

var ErrPermFailSPFFailure = &smtp.SMTPError{
	Code:         510,
	EnhancedCode: smtp.NoEnhancedCode,
	Message:      "SPF check failure",
}

var ErrPermFailDKIMFailure = &smtp.SMTPError{
	Code:         510,
	EnhancedCode: smtp.NoEnhancedCode,
	Message:      "DKIM check failure",
}

var ErrPermDontUnderstandIP = &smtp.SMTPError{
	Code:         510,
	EnhancedCode: smtp.NoEnhancedCode,
	Message:      "I don't understand your IP",
}

var ErrPermFailSpam = &smtp.SMTPError{
	Code:         550,
	EnhancedCode: smtp.EnhancedCode{5, 7, 1},
	Message:      "Spam detected",
}

var ErrPermUserUnknown = &smtp.SMTPError{
	Code:         553,
	EnhancedCode: smtp.NoEnhancedCode,
	Message:      "Invalid user",
}
