package recipientvalidator

import (
	"encoding/json"
	"io"
	"net/http"
	"net/mail"
	"strings"

	"gitlab.com/nerzhul/nsmtpd/internal/common"
	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/codes"

	"github.com/emersion/go-smtp"
	"github.com/go-logr/logr"
)

type delegRcptRequest struct {
	RcptTo string `json:"rcpt-to" query:"rcpt-to"`
}

type ValidatorNImapd struct {
	config Config
}

func NewNImapdValidator(config Config) *ValidatorNImapd {
	return &ValidatorNImapd{
		config: config,
	}
}

func (s *ValidatorNImapd) Validate(to string, logger logr.Logger) *smtp.SMTPError {
	addrParser := mail.AddressParser{}
	if _, err := addrParser.ParseList(to); err != nil {
		logger.Error(err, "Unable to parse mail list")
		return codes.ErrPermFailMalformedMailAddr
	}

	client := &http.Client{}
	rcptTo, err := common.AESEncryptToBase64([]byte(s.config.GetEncryptKey()), []byte(to))
	if err != nil {
		logger.Error(err, "Unable to encrypt RCPT-TO")
		return codes.ErrTempFailUserLookup
	}

	reqJson := &delegRcptRequest{
		RcptTo: rcptTo,
	}

	rjr, err := json.Marshal(reqJson)
	if err != nil {
		logger.Error(err, "Unable to json marshal RCPT-TO encrypted string")
		return codes.ErrTempFailUserLookup
	}

	req, err := http.NewRequest("POST", s.config.GetURL(),
		strings.NewReader(string(rjr)))
	if err != nil {
		logger.Error(err, "Unable to create HTTP request to check recipient")
		return codes.ErrTempFailUserLookup
	}

	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth(s.config.GetUser(), s.config.GetPassword())
	resp, err := client.Do(req)
	if err != nil {
		logger.Error(err, "Unable to perform HTTP request to check recipient")
		return codes.ErrTempFailUserLookup
	}

	if resp == nil {
		logger.Error(err, "Unable to check recipients over HTTP, nil response")
		return codes.ErrTempFailUserLookup
	}

	switch resp.StatusCode {
	case 200:
		logger.Info("recipients validated")
		return nil
	case 400:
		logger.Error(err, "http RCPT-TO validator refused our query (malformed ?)")
		return codes.ErrTempFailUserLookup
	case 404:
		logger.Error(err, "http RCPT-TO validator has no such user")
		return codes.ErrPermUserUnknown
	case 500:
		logger.Error(err, "http RCPT-TO validator has a hard failure (code 500), failed temporary")
		return codes.ErrTempFailUserLookup
	case 501:
		logger.Error(err, "http RCPT-TO validator has a no delegated validation setup, failed temporary")
		return codes.ErrTempFailUserLookup
	default:
		{
			respRaw, err := io.ReadAll(resp.Body)
			if err != nil {
				respRaw = []byte{}
				logger.Error(err, "unable to read body while trying to find check recipients over HTTP")
			}

			logger.WithValues("status", resp.StatusCode, "body", string(respRaw)).Error(err, "recipient check failed")
			return codes.ErrTempFailUserLookup
		}
	}
}
