package recipientvalidator

type Config interface {
	GetMethod() string
	GetURL() string
	GetEncryptKey() string
	GetPassword() string
	GetUser() string
	IsDynamicAliasingEnabled() bool
	GetDynamicAliasingSeparator() rune
	Load() error
	Print()
}