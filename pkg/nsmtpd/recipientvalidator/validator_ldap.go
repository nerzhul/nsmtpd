package recipientvalidator

import (
	"net/mail"

	"gitlab.com/nerzhul/nsmtpd/internal/common"
	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/codes"

	"github.com/emersion/go-smtp"
	"github.com/go-logr/logr"
)

type ValidatorLDAP struct {
	dynAliasEnabled bool
	dynAliasSeparator rune
	ldapConfig common.LDAPConfig
}

func NewLDAPValidator(ldapConfig common.LDAPConfig, config Config) *ValidatorLDAP {
	return &ValidatorLDAP{
		dynAliasEnabled: config.IsDynamicAliasingEnabled(),
		dynAliasSeparator: config.GetDynamicAliasingSeparator(),
		ldapConfig: ldapConfig,
	}
}

func (s *ValidatorLDAP) Validate(to string, logger logr.Logger) *smtp.SMTPError {
	conn := common.NewLDAPConn(&s.ldapConfig)
	if err := conn.Connect(); err != nil {
		logger.Error(err, "Unable to connect to LDAP server")
		return codes.ErrTempFailUserLookup
	}

	defer conn.Close()

	if err := conn.Login(); err != nil {
		logger.Error(err, "Unable to login to LDAP server")
		return codes.ErrTempFailUserLookup
	}

	addrParser := mail.AddressParser{}
	maddrs, err := addrParser.ParseList(to)
	if err != nil {
		logger.Error(err, "Unable to parse mail list")
		return codes.ErrPermFailMalformedMailAddr
	}

	for _, maddr := range maddrs {
		maddr, err = common.ResolveDynamicAliasedMail(maddr,s.dynAliasEnabled, s.dynAliasSeparator)
		logger := logger.WithValues("maddr", maddr)
		if err != nil {
			logger.Error(err, "Unable to resolve dynamic aliased mail")
			return codes.ErrTempFailUserLookup
		}

		username, err := conn.GetUsernameFromMail(maddr.Address)
		if err != nil {
			logger.Error(err, "Unable to fetch username for mail list")
			return codes.ErrTempFailUserLookup
		}

		if username == "" {
			logger.Info("invalid user. Dropping transaction")
			return codes.ErrPermUserUnknown
		}

		logger.WithValues("username", username).Info("valid user mapped")
	}
	return nil
}
