package recipientvalidator

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nerzhul/nsmtpd/internal/common"
)

func TestConfig_Method(t *testing.T) {
	c := &ConfigImpl{
		Method: "http",
	}

	assert.Equal(t, c.GetMethod(), "http")

	// Invalid method
	{
		os.Setenv("RECIPIENTVALIDATOR_METHOD", "smtp")
		c = NewConfigImpl()
		assert.Equal(t, fmt.Errorf("invalid configuration for recipient validator method: 'smtp'. Required method: 'http/ldap'"), c.Load())
	}

	// Missing encrypt key
	{
		os.Setenv("RECIPIENTVALIDATOR_METHOD", "http")
		os.Setenv("RECIPIENTVALIDATOR_URL", "http://localhost")
		c = NewConfigImpl()
		assert.Equal(t, fmt.Errorf("empty recipient validator encrypt key while method is 'http'. Please set it"), c.Load())
	}

	// Missing HTTP URL
	{
		os.Setenv("RECIPIENTVALIDATOR_METHOD", "http")
		os.Setenv("RECIPIENTVALIDATOR_ENCRYPTKEY", "key")
		os.Setenv("RECIPIENTVALIDATOR_URL", "")
		c = NewConfigImpl()
		assert.Equal(t, fmt.Errorf("empty recipient validator URL while method is 'http'. Please set it"), c.Load())
	}
}

func TestConfig_DynamicAliasingConfig(t *testing.T) {
	// Getters
	c := &ConfigImpl{
		DynAliasConfig: common.DynamicAliasingConfig{
			Enable:    true,
			Separator: "-",
		},
	}

	assert.True(t, c.IsDynamicAliasingEnabled())
	assert.Equal(t, c.GetDynamicAliasingSeparator(), '-')

	// Valid config
	{
		os.Setenv("DYNAMICALIASING_ENABLE", "false")
		os.Setenv("DYNAMICALIASING_SEPARATOR", "_")
		c := NewConfigImpl()
		assert.Nil(t, c.Load())
		assert.False(t, c.IsDynamicAliasingEnabled())
		assert.Equal(t, c.GetDynamicAliasingSeparator(), '_')
	}

	// Invalid separator
	{
		os.Setenv("DYNAMICALIASING_ENABLE", "true")
		os.Setenv("DYNAMICALIASING_SEPARATOR", "")
		assert.Equal(t, fmt.Errorf("dynamic aliasing enabled but separator is empty"), c.Load())
	}

	// Invalid separator
	{
		os.Setenv("DYNAMICALIASING_ENABLE", "true")
		os.Setenv("DYNAMICALIASING_SEPARATOR", "aa")
		assert.Equal(t, fmt.Errorf("dynamic aliasing enabled but separator is not a single character: aa"), c.Load())
	}
}
