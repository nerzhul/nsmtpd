package recipientvalidator

import (
	"github.com/emersion/go-smtp"
	"github.com/go-logr/logr"
)

type Validator interface {
	Validate(email string, logger logr.Logger) *smtp.SMTPError
}
