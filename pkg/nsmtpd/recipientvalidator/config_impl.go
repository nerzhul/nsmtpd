package recipientvalidator

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/nerzhul/nsmtpd/internal/common"
	"k8s.io/klog/v2"
)

type ConfigImpl struct {
	Method         string `envconfig:"RECIPIENTVALIDATOR_METHOD"`
	URL            string `envconfig:"RECIPIENTVALIDATOR_URL"`
	EncryptKey     string `envconfig:"RECIPIENTVALIDATOR_ENCRYPTKEY"`
	Password       string `envconfig:"RECIPIENTVALIDATOR_PASSWORD"`
	User           string `envconfig:"RECIPIENTVALIDATOR_USER"`
	DynAliasConfig common.DynamicAliasingConfig
}

func NewConfigImpl() *ConfigImpl {
	return &ConfigImpl{
		DynAliasConfig: common.DynamicAliasingConfig{
			Enable:    false,
			Separator: "+",
		},
		Method:     "http",
		URL:        "",
		EncryptKey: "",
		User:       "",
		Password:   "",
	}
}

func (c *ConfigImpl) GetMethod() string {
	return c.Method
}

func (c *ConfigImpl) GetURL() string {
	return c.URL
}

func (c *ConfigImpl) GetEncryptKey() string {
	return c.EncryptKey
}

func (c *ConfigImpl) GetPassword() string {
	return c.Password
}

func (c *ConfigImpl) GetUser() string {
	return c.User
}

func (c *ConfigImpl) IsDynamicAliasingEnabled() bool {
	return c.DynAliasConfig.Enable
}

func (c *ConfigImpl) GetDynamicAliasingSeparator() rune {
	return rune(c.DynAliasConfig.Separator[0])
}

func (c *ConfigImpl) Load() error {
	err := envconfig.Process("", c)
	if err != nil {
		return err
	}

	if len(c.DynAliasConfig.Separator) == 0 {
		return fmt.Errorf("dynamic aliasing enabled but separator is empty")
	}

	if len(c.DynAliasConfig.Separator) != 1 {
		return fmt.Errorf("dynamic aliasing enabled but separator is not a single character: %s", c.DynAliasConfig.Separator)
	}

	if c.Method != "ldap" && c.Method != "http" {
		return fmt.Errorf("invalid configuration for recipient validator method: '%s'. Required method: 'http/ldap'",
			c.Method)
	}

	if c.Method == "http" && c.EncryptKey == "" {
		return fmt.Errorf("empty recipient validator encrypt key while method is 'http'. Please set it")
	}

	if c.Method == "http" && c.URL == "" {
		return fmt.Errorf("empty recipient validator URL while method is 'http'. Please set it")
	}

	return nil
}

func (c *ConfigImpl) Print() {
	if c.DynAliasConfig.Enable {
		klog.Infof("Dynamic aliasing enabled. Separator: %s", c.DynAliasConfig.Separator)
	}

	klog.Infof("Configured recipient validator URL: %s", c.GetURL())
}
