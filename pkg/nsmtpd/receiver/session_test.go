package receiver

import (
	"net"
	"testing"

	"github.com/go-logr/logr"
	"github.com/stretchr/testify/assert"
)

var (
	sessionTestAddr = net.Addr(&net.TCPAddr{
		IP:   net.ParseIP("127.0.0.1"),
		Port: 25,
	})
)

func TestSession_checkMX(t *testing.T) {
	s := Session{
		addr: sessionTestAddr,
	}

	assert.NotNil(t, s.checkMxForDomain("dqmsdlq.roerpeor00éà", logr.Logger{}))
	assert.Nil(t, s.checkMxForDomain("google.com", logr.Logger{}))
}

func TestSession_checkSPF(t *testing.T) {
	// TODO: rewrite this test without SPF real checking
	// s := Session{
	// 	context:  context.WithValue(context.TODO(), "addr", "136.147.188.171:55478"),
	// 	mailFrom: "bounce-396_HTML-114816134-355177-7231766-20699@bounce.email.leetchi.com",
	// }

	// s.context = context.WithValue(s.context, "command", "unittests")

	// assert.Nil(t, s.checkMailSPF())

	// s2 := Session{
	// 	context:  context.WithValue(context.TODO(), "addr", "136.147.188.171"),
	// 	mailFrom: "bounce-396_HTML-114816134-355177-7231766-20699@bounce.email.leetchi.com",
	// }

	// s2.context = context.WithValue(s2.context, "command", "unittests")

	// assert.Nil(t, s2.checkMailSPF())
}

func TestSession_checkDKIM(t *testing.T) {
	s := Session{
		mailFrom: "bounce-396_HTML-114816134-355177-7231766-20699@bounce.email.leetchi.com",
		addr:     sessionTestAddr,
	}

	// Check with no DKIM signature
	assert.Nil(t, s.checkMailDKIM("From: Sender's Alias <Sender's reply e-mail address>\nTo: Receiver's Alias <Recipient's e-mail address>\nSubject: <Subject Goes Here>\n\n<body goes here>", logr.Logger{}))

	// TODO find a DKIM valid message we can use for unittests
}

func TestSession_getDomainFromAddress(t *testing.T) {
	s := Session{
		addr: sessionTestAddr,
	}

	m, err := s.getDomainFromAddress("pouet")
	assert.NotNil(t, err)
	assert.Empty(t, m)

	m, err = s.getDomainFromAddress("pouet@example.org")
	assert.Nil(t, err)
	assert.NotEmpty(t, m)
	assert.Equal(t, "example.org", m)

	m, err = s.getDomainFromAddress(`"pouet" pouet@example.org>`)
	assert.Nil(t, err)
	assert.NotEmpty(t, m)
	assert.Equal(t, "example.org", m)

	m, err = s.getDomainFromAddress(`"pouet" <pouet@example.org>`)
	assert.Nil(t, err)
	assert.NotEmpty(t, m)
	assert.Equal(t, "example.org", m)
}

func TestSession_getMailFromAddress(t *testing.T) {
	s := Session{
		addr: sessionTestAddr,
	}

	m, err := s.getMailFromAddress("pouet")
	assert.NotNil(t, err)
	assert.Empty(t, m)

	m, err = s.getMailFromAddress("pouet@example.org")
	assert.Nil(t, err)
	assert.NotEmpty(t, m)
	assert.Equal(t, "pouet@example.org", m)

	m, err = s.getMailFromAddress(`"pouet" pouet@example.org>`)
	assert.NotNil(t, err)
	assert.Empty(t, m)

	m, err = s.getMailFromAddress(`"pouet" <pouet@example.org`)
	assert.NotNil(t, err)
	assert.Empty(t, m)

	m, err = s.getMailFromAddress(`"pouet" <pouet@example.org>`)
	assert.Nil(t, err)
	assert.NotEmpty(t, m)
	assert.Equal(t, "pouet@example.org", m)
}
