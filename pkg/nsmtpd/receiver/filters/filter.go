package filters

import (
	"net/mail"

	"github.com/go-logr/logr"
)

type Filter interface {
	Name() string
	Apply(m *mail.Message, logger logr.Logger) error
}
