package filters

import (
	"fmt"
	"net/mail"
	"regexp"

	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/codes"

	"github.com/go-logr/logr"
)

type HeaderContentFilter struct {
	header            string
	regexRestrictions []regexp.Regexp
}

func NewHeaderContentFilter(header string, restrictionsRegexpStr []string) *HeaderContentFilter {
	compiledRestrictions := make([]regexp.Regexp, 0, len(restrictionsRegexpStr))
	for _, restriction := range restrictionsRegexpStr {
		compiledRestrictions = append(compiledRestrictions, *regexp.MustCompile(restriction))
	}

	return &HeaderContentFilter{
		header:            header,
		regexRestrictions: compiledRestrictions,
	}
}

func (f *HeaderContentFilter) Name() string {
	return fmt.Sprintf("HeaderContentFilter (header: %s)", f.header)
}

func (f *HeaderContentFilter) Apply(m *mail.Message, logger logr.Logger) error {
	// header not found, ignore
	headerValue := m.Header.Get(f.header)
	if headerValue == "" {
		return nil
	}

	for _, restriction := range f.regexRestrictions {
		if restriction.MatchString(headerValue) {
			logger.Error(fmt.Errorf("restricted %s '%s' found in mail content", f.header, headerValue), "data command failed (filter failed)")
			return codes.ErrPermFailSpam
		}
	}

	return nil
}

type MandatoryHeaderFilter struct {
	headers []string
}

func NewMandatoryHeaderFilter(headers []string) *MandatoryHeaderFilter {
	return &MandatoryHeaderFilter{headers: headers}
}

func (f *MandatoryHeaderFilter) Name() string {
	return "MandatoryHeaderFilter"
}

func (f *MandatoryHeaderFilter) Apply(m *mail.Message, logger logr.Logger) error {
	for _, header := range f.headers {
		if m.Header.Get(header) == "" {
			logger.Error(fmt.Errorf("mandatory header '%s' not found in mail content", header), "data command failed (mandatory-header-check failed)")
			return codes.ErrPermFailSpam
		}
	}

	return nil
}

type ForbiddenHeaderFilter struct {
	headers []string
}

func NewForbiddenHeaderFilter(headers []string) *ForbiddenHeaderFilter {
	return &ForbiddenHeaderFilter{headers: headers}
}

func (f *ForbiddenHeaderFilter) Name() string {
	return "ForbiddenHeaderFilter"
}

func (f *ForbiddenHeaderFilter) Apply(m *mail.Message, logger logr.Logger) error {
	for _, header := range f.headers {
		if m.Header.Get(header) != "" {
			logger.Error(fmt.Errorf("forbidden header '%s' found in mail content", header), "data command failed (forbidden-header-check failed)")
			return codes.ErrPermFailSpam
		}
	}

	return nil
}

type WhiteListHeaderFilter struct {
	header         string
	regexWhitelist []regexp.Regexp
}

func NewWhiteListHeaderFilter(header string, whitelistRegexpStr []string) *WhiteListHeaderFilter {
	compiledWhitelist := make([]regexp.Regexp, 0, len(whitelistRegexpStr))
	for _, whitelist := range whitelistRegexpStr {
		compiledWhitelist = append(compiledWhitelist, *regexp.MustCompile(whitelist))
	}

	return &WhiteListHeaderFilter{
		header:         header,
		regexWhitelist: compiledWhitelist,
	}
}

func (f *WhiteListHeaderFilter) Name() string {
	return "WhiteListHeaderFilter"
}

func (f *WhiteListHeaderFilter) Apply(m *mail.Message, logger logr.Logger) error {
	headerValue := m.Header.Get(f.header)
	for _, whitelistRegex := range f.regexWhitelist {
		if whitelistRegex.MatchString(headerValue) {
			return nil
		}
	}

	logger.Error(fmt.Errorf("%s '%s' not found in whitelist", f.header, headerValue), "data command failed (whitelist-to-check failed)")
	return codes.ErrPermFailSpam
}

type DestinationDomainWhitelist struct {
	regexWhitelist []regexp.Regexp
}

func NewDestinationDomainWhitelist(whitelistRegexpStr []string) *DestinationDomainWhitelist {
	compiledWhitelist := make([]regexp.Regexp, 0, len(whitelistRegexpStr))
	for _, whitelist := range whitelistRegexpStr {
		compiledWhitelist = append(compiledWhitelist, *regexp.MustCompile(whitelist))
	}

	return &DestinationDomainWhitelist{
		regexWhitelist: compiledWhitelist,
	}
}

func (f *DestinationDomainWhitelist) Name() string {
	return "DestinationDomainWhitelist"
}

func (f *DestinationDomainWhitelist) Apply(m *mail.Message, logger logr.Logger) error {
	toHeaderValue := m.Header.Get("To")
	ccHeaderValue := m.Header.Get("Cc")
	bccHeaderValue := m.Header.Get("Bcc")

	for _, whitelistRegex := range f.regexWhitelist {
		if whitelistRegex.MatchString(toHeaderValue) ||
			whitelistRegex.MatchString(ccHeaderValue) ||
			whitelistRegex.MatchString(bccHeaderValue) {
			return nil
		}
	}

	logger.Error(fmt.Errorf("destination domain not found in whitelist"), "data command failed (destination-domain-check failed)")
	return codes.ErrPermFailSpam
}
