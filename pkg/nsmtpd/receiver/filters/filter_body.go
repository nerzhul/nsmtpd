package filters

import (
	"bytes"
	"fmt"
	"net/mail"
	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/codes"
	"regexp"

	"github.com/go-logr/logr"
)

type BodyFilter struct {
	restrictedKeywords []string
}

func NewBodyFilter(restrictedKeywords []string) *BodyFilter {
	return &BodyFilter{restrictedKeywords: restrictedKeywords}
}

func (f *BodyFilter) Name() string {
	return "BodyFilter"
}

func (f *BodyFilter) Apply(m *mail.Message, logger logr.Logger) error {
	var bodyBuffer bytes.Buffer
	_, err := bodyBuffer.ReadFrom(m.Body)
	if err != nil {
		logger.Error(err, "data command failed (unable to read mail body)")
		return codes.ErrTempFailPermCheck
	}

	for _, keyword := range f.restrictedKeywords {
		matched, err := regexp.MatchString(keyword, bodyBuffer.String())
		if err != nil {
			panic(err)
		}

		if matched {
			logger.Error(fmt.Errorf("keyword '%s' found in mail content", keyword), "data command failed (keyword-check failed)")
			return codes.ErrPermFailSpam
		}
	}

	return nil
}
