package filters

import (
	"net/mail"

	"github.com/go-logr/logr"
)

type FilterBackend interface {
	Name() string
	FilterCount() int
	ApplyOn_DataCommand(m *mail.Message, logger logr.Logger) error
}
