package receiver

import (
	"fmt"
	"net"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/codes"
	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/config"
	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/recipientvalidator"

	"github.com/emersion/go-smtp"
	"github.com/go-logr/logr"
	"github.com/google/uuid"
	"github.com/mrichman/godnsbl"
	"github.com/patrickmn/go-cache"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"k8s.io/klog/v2"
)

// The Backend implements SMTP server methods.
type Backend struct {
	config *config.ConfigImpl

	cache  *cache.Cache
	logger logr.Logger
}

var (
	cmdLoginMetric              prometheus.Counter
	cmdLoginTimeMetric          prometheus.Counter
	cmdAnonymousLoginMetric     prometheus.Counter
	cmdAnonymousLoginTimeMetric prometheus.Counter
	cmdDNSBLCheckMetric         prometheus.Counter
	cmdDNSBLHitMetric           prometheus.Counter
	cmdDNSBLRejectMetric        prometheus.Counter
)

func init() {
	cmdLoginMetric = promauto.NewCounter(prometheus.CounterOpts{
		Name: "nsmtpd_receiver_command_login_total",
		Help: "The total number of receiver login command",
	})
	cmdLoginTimeMetric = promauto.NewCounter(prometheus.CounterOpts{
		Name: "nsmtpd_receiver_command_login_total_time",
		Help: "The total number of receiver login command exec time (in ns)",
	})
	cmdAnonymousLoginMetric = promauto.NewCounter(prometheus.CounterOpts{
		Name: "nsmtpd_receiver_command_anonymous_login_total",
		Help: "The total number of receiver anonymous login command",
	})
	cmdAnonymousLoginTimeMetric = promauto.NewCounter(prometheus.CounterOpts{
		Name: "nsmtpd_receiver_command_anonymous_login_total_time",
		Help: "The total number of receiver anonymous login command exec time (in ns)",
	})
	cmdDNSBLCheckMetric = promauto.NewCounter(prometheus.CounterOpts{
		Name: "nsmtpd_receiver_command_dnsbl_check_total",
		Help: "The total number of receiver DNSBL checks",
	})
	cmdDNSBLHitMetric = promauto.NewCounter(prometheus.CounterOpts{
		Name: "nsmtpd_receiver_command_dnsbl_hits_total",
		Help: "The total number of receiver DNSBL hits",
	})
	cmdDNSBLRejectMetric = promauto.NewCounter(prometheus.CounterOpts{
		Name: "nsmtpd_receiver_command_dnsbl_reject_total",
		Help: "The total number of receiver DNSBL rejects",
	})
}

func newBackend(c *config.ConfigImpl) *Backend {
	return &Backend{
		config: c,
		cache:  cache.New(time.Duration(c.Receiver().GetDNSBLCacheTTL())*time.Second, 1*time.Minute),
		logger: klog.NewKlogr(),
	}
}

func (b *Backend) NewSession(c *smtp.Conn) (smtp.Session, error) {
	// generate session ID from UUID
	sessionID := uuid.NewString()

	logger := b.logger.WithValues(
		"session-id", sessionID,
		"remote-addr", c.Conn().RemoteAddr(),
		"smtp-hostname", c.Hostname(),
	)

	logger.Info("new session created")

	// RBL check
	if err := b.performRBLCheck(c.Conn().RemoteAddr(), logger); err != nil {
		klog.ErrorS(err, "RBL check failed", "ip", c.Conn().RemoteAddr())
		return nil, err
	}

	var recipientValidator recipientvalidator.Validator
	switch b.config.GetRecipientValidator().GetMethod() {
	case "ldap":
		recipientValidator = recipientvalidator.NewLDAPValidator(b.config.LDAP, b.config.GetRecipientValidator())
	case "http":
		recipientValidator = recipientvalidator.NewNImapdValidator(b.config.GetRecipientValidator())
	default:
		klog.Fatalf("invalid recipient validator method: %s", b.config.GetRecipientValidator().GetMethod())
	}

	return NewSession(b.config, c.Conn().RemoteAddr(), recipientValidator, logger), nil
}

func (b *Backend) performRBLCheck(conn net.Addr, logger logr.Logger) error {
	ipAddr := ""
	if addr, ok := conn.(*net.TCPAddr); ok {
		ipAddr = addr.IP.String()
	} else {
		return codes.ErrPermDontUnderstandIP
	}

	logger.Info("performing RBL check")
	defer logger.Info("RBL check done")

	// First check if it's in our local cache
	cacheKey := fmt.Sprintf("rbl-blacklisted-%s", ipAddr)
	cachedRBLResult, cacheFound := b.cache.Get(cacheKey)
	if cacheFound {
		switch blacklisted := cachedRBLResult.(type) {
		case bool:
			if blacklisted {
				logger.Info("RBL check", "result", "rejected", "reason", "blacklisted (cached)")
				cmdDNSBLRejectMetric.Inc()
				return codes.ErrPermFailSpam
			}

			logger.Info("RBL check", "result", "passed", "cached", "true")
			return nil
		default:
			logger.Error(fmt.Errorf("invalid cache entry type: %T", cachedRBLResult), "RBL check")
			panic("invalid cache entry type")
		}
	}

	dnsBLHits := atomic.Uint32{}
	var wg sync.WaitGroup

	listedRBLs := make([]string, 0)
	reasons := make([]string, 0)

	for _, source := range godnsbl.Blacklists {
		wg.Add(1)

		go func(source string, ipAddr string) {
			defer wg.Done()

			cmdDNSBLCheckMetric.Inc()

			rbl := godnsbl.Lookup(source, ipAddr)

			if len(rbl.Results) > 0 {
				for _, r := range rbl.Results {
					if r.Listed {
						dnsBLHits.Add(1)
						cmdDNSBLHitMetric.Inc()
						listedRBLs = append(listedRBLs, rbl.List)
						reasons = append(reasons, r.Text)
					}
				}
			}
		}(source, ipAddr)
	}

	wg.Wait()

	if int(dnsBLHits.Load()) >= b.config.Receiver().GetDNSBLMinHits() {
		logger.Info("DNS RBL check", "ip", ipAddr, "hits", dnsBLHits.Load(), "result", "rejected", "reason", "min-hits-reached", "rbls", listedRBLs, "reasons", reasons)
		b.cache.Set(cacheKey, true, time.Duration(b.config.Receiver().GetDNSBLCacheTTL())*time.Second)
		cmdDNSBLRejectMetric.Inc()
		return codes.ErrPermFailSpam
	}

	logger.Info("DNS RBL check", "ip", ipAddr, "hits", dnsBLHits.Load(), "result", "passed", "cached", "false")
	b.cache.Set(cacheKey, false, time.Duration(b.config.Receiver().GetDNSBLCacheTTL())*time.Minute)

	return nil
}
