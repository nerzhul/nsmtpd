package receiver

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/mail"
	"net/url"
	"strings"

	"gitlab.com/nerzhul/nsmtpd/internal/common"
	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/codes"
	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/config"
	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/recipientvalidator"

	"github.com/emersion/go-msgauth/dkim"
	"github.com/emersion/go-smtp"
	"github.com/go-logr/logr"
	"github.com/nerzhul/spf"
)

// A Session is returned after successful login.
type Session struct {
	config             config.Config
	addrParser         mail.AddressParser
	recipientValidator recipientvalidator.Validator
	mailFrom           string
	rcptTo             string
	addr               net.Addr
	logger             logr.Logger
}

func NewSession(c config.Config, addr net.Addr, recipientValidator recipientvalidator.Validator, logger logr.Logger) *Session {
	return &Session{
		config:             c,
		addrParser:         mail.AddressParser{},
		addr:               addr,
		logger:             logger,
		recipientValidator: recipientValidator,
	}
}

func (s *Session) AuthPlain(username, password string) error {
	s.logger.WithValues("command", "auth-plain", "username", username).Info("auth-plain command")

	return smtp.ErrAuthUnsupported
}

func (s *Session) Mail(from string, opts *smtp.MailOptions) error {
	logger := s.logger.WithValues("command", "mail-from", "from", from)
	logger.Info("mail-from command")

	if s.config.Receiver().IsMXValidationEnabled() {
		if err := s.checkMailFromMX(from, logger); err != nil {
			// Logging already done in checkMailFromMX
			return err
		}

		logger.Info("mail-from mx validation succeeded")
	}

	s.mailFrom = from

	if s.config.Receiver().IsMXValidationEnabled() {
		if err := s.checkMailSPF(logger); err != nil {
			return err
		}
	}

	if restricted, err := s.config.Receiver().IsRestrictedFromDomain([]byte(s.mailFrom)); restricted && err == nil {
		logger.Error(err, "mail-from domain check restriction, discard")
		return codes.ErrPermFailSpam
	} else if err != nil {
		logger.Error(err, "mail-from domain check failure")
		return codes.ErrTempFailPermCheck
	}

	s.logger.Info("mail-from command ok")
	return nil
}

func (s *Session) Rcpt(to string, opts *smtp.RcptOptions) error {
	logger := s.logger.WithValues("command", "rcpt-to", "to", to)
	logger.Info("rcpt-to command")

	if err := s.recipientValidator.Validate(to, logger); err != nil {
		logger.Error(err, "rcpt-to command failed (rcpt-validator failed)")
	}

	s.rcptTo = to

	logger.Info("rcpt-to command ok")
	return nil
}

func (s *Session) Data(r io.Reader) error {
	logger := s.logger.WithValues("command", "data")
	b, err := io.ReadAll(r)
	if err != nil {
		logger.Error(err, "data command failed (unable to read data)")
		return err
	}

	if s.config.Receiver().IsDKIMValidationEnabled() {
		if err := s.checkMailDKIM(string(b), logger); err != nil {
			logger.Error(err, "data command failed (dkim-check failed)")
			return err
		}
	}

	m, err := mail.ReadMessage(bytes.NewReader(b))
	if err != nil {
		logger.Error(err, "data command failed (unable to parse mail content)")
		return codes.ErrTempFailPermCheck
	}

	if err := s.config.Receiver().GetFilteringBackend().ApplyOn_DataCommand(m, logger); err != nil {
		logger.Error(err, "data command failed (filter-check failed)")
		return err
	}

	logger.Info("data command ok")
	return s.pushToMailBackend(b)
}

func (s *Session) checkMailFromMX(to string, logger logr.Logger) error {
	maddrs, err := s.addrParser.ParseList(to)
	if err != nil {
		logger.Error(err, "mail-from command failed (mx-check failed)")
		return codes.ErrPermFailMalformedMailAddr
	}

	if len(maddrs) != 1 {
		logger.Error(fmt.Errorf("addr count %d!=1 addrs=%v", len(maddrs), maddrs), "mail-from command failed (mx-check failed)")
		return codes.ErrPermFailMalformedMailAddr
	}

	for _, maddr := range maddrs {
		llogger := logger.WithValues("maddr", maddr.Address)
		addr, err := s.getDomainFromAddress(maddr.String())
		if err != nil {
			llogger.Error(err, "mail-from command failed (mx-check failed - invalid address)")
			return codes.ErrPermFailMalformedMailAddr
		}

		if err := s.checkMxForDomain(addr, llogger); err != nil {
			// logging done in function
			return err
		}
	}
	return nil
}

func (s *Session) getDomainFromAddress(m string) (string, error) {
	splitAddr := strings.Split(m, "@")
	switch len(splitAddr) {
	case 2:
		// Drop the mail markers
		addr := strings.ReplaceAll(splitAddr[1], "<", "")
		return strings.ReplaceAll(addr, ">", ""), nil
	default:
		return "", codes.ErrPermFailMalformedMailAddr
	}
}

func (s *Session) getMailFromAddress(m string) (string, error) {
	if !strings.Contains(m, "@") {
		return "", codes.ErrPermFailMalformedMailAddr
	}

	if strings.Contains(m, "<") {
		if !strings.Contains(m, ">") {
			return "", codes.ErrPermFailMalformedMailAddr
		}

		splitAddr := strings.Split(m, "<")
		switch len(splitAddr) {
		case 2:
			return strings.ReplaceAll(splitAddr[1], ">", ""), nil
		default:
			return "", codes.ErrPermFailMalformedMailAddr
		}
	}

	if strings.Contains(m, ">") && !strings.Contains(m, "<") {
		return "", codes.ErrPermFailMalformedMailAddr
	}

	return m, nil
}

func (s *Session) checkMxForDomain(domain string, logger logr.Logger) *smtp.SMTPError {
	logger = logger.WithValues("domain", domain)
	// perform the test
	mxList, err := net.LookupMX(domain)
	if err != nil {
		logger.Error(err, "mail-from command failed (mx-check failed - unable to lookup MX)")
		return codes.ErrTempFailPermCheck
	}

	if len(mxList) == 0 {
		logger.Error(err, "mail-from command failed (mx-check failed - no MX found)")
		return codes.ErrPermFailNoMX
	}

	logger.V(10).Info("MX list for domain %s: %v", domain, mxList)
	logger.Info("MX list is valid.", domain)
	return nil
}

func (s *Session) checkMailSPF(logger logr.Logger) error {
	ipAddr := s.addr.String()
	// Split if the port is included in field
	ipAddr = strings.Split(ipAddr, ":")[0]

	addr, err := s.getMailFromAddress(s.mailFrom)
	if err != nil {
		logger.Error(err, "spf-check failed - invalid address")
		return err
	}

	result, err := spf.SPFTest(ipAddr, addr)
	if err != nil {
		logger.Info("spf verification check technically failed, send a TempFail status: %s", err.Error())
		return codes.ErrTempFailPermCheck
	}

	if result == spf.TempError {
		logger.Info("temporary SPF failure, send a TempFail status")
		return codes.ErrTempFailPermCheck
	}

	if result == spf.Fail || result == spf.PermError {
		logger.WithValues("result", result, "ipaddr", ipAddr, "maddr", addr).Error(err, "spf check failed, discard")
		return codes.ErrPermFailSPFFailure
	}

	logger.Info("spf verification succeed")
	return nil
}

func (s *Session) checkMailDKIM(rawMail string, logger logr.Logger) error {
	r := strings.NewReader(rawMail)

	ipAddr := s.addr.String()
	// Split if the port is included in field
	ipAddr = strings.Split(ipAddr, ":")[0]

	logger = logger.WithValues("ipaddr", ipAddr)

	addr, err := s.getMailFromAddress(s.mailFrom)
	if err != nil {
		logger.Error(err, "dkim-check failed - invalid address")
		return err
	}

	logger = logger.WithValues("maddr", addr)

	verifications, err := dkim.Verify(r)
	if err != nil {
		logger.Error(err, "dkim-check failed (verify)")
		return codes.ErrPermFailDKIMFailure
	}

	for _, v := range verifications {
		if v.Err != nil {
			logger.Error(v.Err, "dkim-check failed (signature)")
			return codes.ErrPermFailDKIMFailure
		}
	}

	logger.Info("dkim verification succeed")
	return nil
}

func (s *Session) pushToMailBackend(data []byte) error {
	urlCfg := s.config.GetMailBackend().GetURL()
	u, err := url.Parse(urlCfg)
	if err != nil {
		return fmt.Errorf("invalid mail backend url %s: %s", urlCfg, err)
	}

	switch u.Scheme {
	case "lmtp":
		return s.pushToLMTP(u.Host, data)
	case "http":
		return s.pushToHTTP(urlCfg, data)
	case "https":
		return s.pushToHTTP(urlCfg, data)
	}

	return fmt.Errorf("invalid mail backend scheme %s", u.Scheme)
}

func (s *Session) pushToLMTP(host string, data []byte) error {
	conn, err := net.Dial("tcp", host)
	if err != nil {
		s.logger.Error(err, "Unable to create LMTP tcp conn")
		return codes.ErrTempFailUserDelivery
	}

	client := smtp.NewClientLMTP(conn)
	defer client.Close()

	if err := client.Hello(s.config.GetHostname()); err != nil {
		s.logger.Error(err, "Unable to hello LMTP server")
		return codes.ErrTempFailUserDelivery
	}

	if err := client.Mail(s.mailFrom, &smtp.MailOptions{}); err != nil {
		s.logger.Error(err, "Unable to MAIL FROM LMTP server")
		return codes.ErrTempFailUserDelivery
	}

	if err := client.Rcpt(s.rcptTo, &smtp.RcptOptions{}); err != nil {
		s.logger.Error(err, "Unable to RCPT TO LMTP server")
		return codes.ErrTempFailUserDelivery
	}

	w, err := client.Data()
	if err != nil {
		s.logger.Error(err, "Unable to DATA to LMTP server")
		return codes.ErrTempFailUserDelivery
	}

	if n, err := w.Write(data); err != nil {
		s.logger.WithValues("bytes-written", n, "bytes-total", len(data)).Error(err, "Unable to write to LMTP stream")
		return codes.ErrTempFailUserDelivery
	}

	if _, err := w.Write([]byte(".\n")); err != nil {
		s.logger.Error(err, "Unable to write ending dot to LMTP stream")
		return codes.ErrTempFailUserDelivery
	}

	if err := w.Close(); err != nil {
		s.logger.Error(err, "Unable to close LMTP stream")
		return codes.ErrTempFailUserDelivery
	}

	if err := client.Quit(); err != nil {
		s.logger.Error(err, "Unable to QUIT to LMTP server")
		return codes.ErrTempFailUserDelivery
	}

	return nil
}

func (s *Session) pushToHTTP(host string, data []byte) error {
	client := &http.Client{}
	b, err := json.Marshal(common.ImapRelayMailBody{MailContent: base64.StdEncoding.EncodeToString(data)})
	if err != nil {
		s.logger.Error(err, "Unable to marshal mail content")
		return codes.ErrTempFailUserDelivery
	}

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/v1/mail/deliver/%s", host, s.rcptTo),
		bytes.NewBuffer(b))
	if err != nil {
		s.logger.Error(err, "Unable to create HTTP request")
		return codes.ErrTempFailUserDelivery
	}

	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth(s.config.GetMailBackend().GetUsername(), s.config.GetMailBackend().GetPassword())
	resp, err := client.Do(req)
	if err != nil {
		s.logger.Error(err, "Unable to push mail to HTTP backend")
		return codes.ErrTempFailUserDelivery
	}

	if resp == nil {
		s.logger.Error(err, "unable to push, nil response")
		return codes.ErrTempFailUserDelivery
	}

	if resp.StatusCode != 200 {
		respRaw, err := io.ReadAll(resp.Body)
		if err != nil {
			respRaw = []byte{}
			s.logger.Error(err, "unable to read body while trying to find error on push")
		}

		s.logger.WithValues("status-code", resp.StatusCode, "body", string(respRaw)).Error(err, "unable to push mail to HTTP backend")
		return codes.ErrTempFailUserDelivery
	}

	s.logger.WithValues("rcpt-to", s.rcptTo).Info("mail pushed to user")
	return nil
}

func (s *Session) Reset() {}

func (s *Session) Logout() error {
	s.logger.WithValues("command", "logout").Info("logout command")
	return nil
}
