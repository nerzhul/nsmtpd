package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfig_IsRestrictedFromDomain(t *testing.T) {
	c := &ReceiverEnvConfig{
		Restrictions: &ReceiverConfigRestrictions{
			FromDomainBlacklist: []string{"example\\.com", "^example\\.org$", "p.toto\\.com"},
		},
	}

	{
		r, err := c.IsRestrictedFromDomain([]byte("example.com"))
		if assert.Nil(t, err) {
			assert.True(t, r)
		}
	}

	{
		r, err := c.IsRestrictedFromDomain([]byte("example.org"))
		if assert.Nil(t, err) {
			assert.True(t, r)
		}
	}

	{
		r, err := c.IsRestrictedFromDomain([]byte("example.net"))
		if assert.Nil(t, err) {
			assert.False(t, r)
		}
	}

	{
		r, err := c.IsRestrictedFromDomain([]byte("patoto.com"))
		if assert.Nil(t, err) {
			assert.True(t, r)
		}
	}

	{
		r, err := c.IsRestrictedFromDomain([]byte("pa.toto.com"))
		if assert.Nil(t, err) {
			assert.False(t, r)
		}
	}

	{
		r, err := c.IsRestrictedFromDomain([]byte("p.totoacom"))
		if assert.Nil(t, err) {
			assert.False(t, r)
		}
	}

	{
		r, err := c.IsRestrictedFromDomain([]byte("pouet.example.com"))
		if assert.Nil(t, err) {
			assert.True(t, r)
		}
	}

	{
		r, err := c.IsRestrictedFromDomain([]byte("pouet.example.org"))
		if assert.Nil(t, err) {
			assert.False(t, r)
		}
	}
}
