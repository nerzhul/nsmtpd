package config

import (
	"regexp"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/receiver/filters"
	"k8s.io/klog/v2"
)

type ReceiverEnvConfig struct {
	Restrictions     *ReceiverConfigRestrictions
	filteringBackend *ConfigFilterBackend
}

func NewReceiverEnvConfig() *ReceiverEnvConfig {
	return &ReceiverEnvConfig{
		Restrictions:     NewReceiverConfigRestrictions(),
		filteringBackend: &ConfigFilterBackend{},
	}
}

func (r *ReceiverEnvConfig) Name() string {
	return "environment"
}

func (r *ReceiverEnvConfig) Print() {
	klog.Infof("Built %d filters using backend %s", r.filteringBackend.FilterCount(), r.filteringBackend.Name())

	klog.Info("Enabled restrictions:")
	if r.Restrictions.SPFValidation {
		klog.Info("* SPF validation")
	}

	if r.Restrictions.MXValidation {
		klog.Info("* MX validation")
	}

	if r.Restrictions.DNSBLMinHits > 0 {
		klog.Infof("* DNS blacklist min hits: %d", r.Restrictions.DNSBLMinHits)
	}

	if len(r.Restrictions.FromDomainBlacklist) > 0 {
		klog.Infof("* From domain blacklist: %v", r.Restrictions.FromDomainBlacklist)
	}

	if len(r.Restrictions.RestrictedKeywords) > 0 {
		klog.Infof("* Restricted keywords: %v", r.Restrictions.RestrictedKeywords)
	}

	if len(r.Restrictions.RestrictedHeaderReplyTo) > 0 {
		klog.Infof("* Restricted Reply-To: %v", r.Restrictions.RestrictedHeaderReplyTo)
	}

	if len(r.Restrictions.MandatoryHeaders) > 0 {
		klog.Infof("* Mandatory headers: %v", r.Restrictions.MandatoryHeaders)
	}

	if len(r.Restrictions.ForbiddenHeaders) > 0 {
		klog.Infof("* Forbidden headers: %v", r.Restrictions.ForbiddenHeaders)
	}

	if len(r.Restrictions.RestrictedHeaderTo) > 0 {
		klog.Infof("* Restricted Header To: %v", r.Restrictions.RestrictedHeaderTo)
	}

	if len(r.Restrictions.DestDomainWhitelist) > 0 {
		klog.Infof("* Destination domain whitelist : %v", r.Restrictions.DestDomainWhitelist)
	}

	klog.Infof("* DNS blacklist cache TTL: %d seconds", r.Restrictions.DNSBLCacheTTL)
}

func (r *ReceiverEnvConfig) Load() error {
	err := envconfig.Process("", r)
	if err != nil {
		return err
	}

	r.filteringBackend.Build(r.Restrictions)
	return nil
}

func (r *ReceiverEnvConfig) IsMXValidationEnabled() bool {
	return r.Restrictions.MXValidation
}

func (r *ReceiverEnvConfig) IsSPFValidationEnabled() bool {
	return r.Restrictions.SPFValidation
}

func (r *ReceiverEnvConfig) IsDKIMValidationEnabled() bool {
	return r.Restrictions.DKIMValidation
}

func (r *ReceiverEnvConfig) IsRestrictedFromDomain(domain []byte) (bool, error) {
	for _, d := range r.Restrictions.FromDomainBlacklist {
		if ok, err := regexp.Match(d, domain); ok && err == nil {
			return true, nil
		} else if err != nil {
			return false, err
		}
	}
	return false, nil
}

func (r *ReceiverEnvConfig) GetDNSBLMinHits() int {
	return r.Restrictions.DNSBLMinHits
}

func (r *ReceiverEnvConfig) GetDNSBLCacheTTL() int {
	return r.Restrictions.DNSBLCacheTTL
}

func (r *ReceiverEnvConfig) GetFilteringBackend() filters.FilterBackend {
	return r.filteringBackend
}
