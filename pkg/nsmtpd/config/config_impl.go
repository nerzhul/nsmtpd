package config

import (
	"fmt"

	"gitlab.com/nerzhul/nsmtpd/internal/common"
	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/recipientvalidator"

	"github.com/kelseyhightower/envconfig"
	"k8s.io/klog/v2"
)

type ConfigImpl struct {
	ReceiverPort        uint16
	RelayPort           uint16
	Hostname            string
	ReadTimeoutSeconds  uint16
	WriteTimeoutSeconds uint16
	MaxMessageSizeMB    int
	MaxRecipients       int
	TLS                 struct {
		KeyPath  string
		CertPath string
	}

	HTTP struct {
		Port uint16
	}

	LDAP        common.LDAPConfig
	MailBackend ConfigMailBackend

	RecipientValidator recipientvalidator.Config
	receiverConfig     ReceiverConfig
}

func (c *ConfigImpl) loadConfigFromEnv() {
	err := envconfig.Process("", c)
	if err != nil {
		klog.Fatal(err.Error())
	}
}

func (c *ConfigImpl) GetReceiverListeningAddr() string {
	return fmt.Sprintf(":%d", c.ReceiverPort)
}

func (c *ConfigImpl) GetRelayListeningAddr() string {
	return fmt.Sprintf(":%d", c.RelayPort)
}

func (c *ConfigImpl) Load() {
	c.ReceiverPort = 1025
	c.RelayPort = 1587
	c.HTTP.Port = 8025
	c.Hostname = "localhost"
	c.ReadTimeoutSeconds = 30
	c.WriteTimeoutSeconds = 30
	c.MaxMessageSizeMB = 10
	c.MaxRecipients = 50

	c.RecipientValidator = recipientvalidator.NewConfigImpl()
	if err := c.RecipientValidator.Load(); err != nil {
		klog.Fatalf("Failed to load recipient validator configuration: %s", err)
	}

	c.RecipientValidator.Print()

	c.LDAP.URL = "ldap://localhost"

	// For relay auth test
	c.LDAP.UserPattern = "uid=%s,ou=people,dc=example,dc=org"

	// For email recv test
	c.LDAP.BindDN = "cn=read-only,ou=people,dc=example,dc=org"
	c.LDAP.BindPassword = "password"
	c.LDAP.BaseDN = "dc=example,dc=org"
	c.LDAP.DomainFilter = "(&(objectClass=domain)(dc=%s))"
	c.LDAP.DomainAttr = "dc"
	c.LDAP.AliasFilter = "(&(objectClass=nisMailAlias)(cn=%s))"
	c.LDAP.AliasAttr = "rfc822MailMember"

	c.MailBackend.URL = "lmtp://127.0.0.1:24"

	c.loadConfigFromEnv()

	c.receiverConfig = NewReceiverEnvConfig()
	if err := c.receiverConfig.Load(); err != nil {
		klog.Fatalf("Failed to load receiver configuration: %s", err)
	}
	c.receiverConfig.Print()

	klog.Info("Configuration loaded.")
	if c.TLS.CertPath != "" {
		klog.Infof("Configured SSL certificate: %s", c.TLS.CertPath)
	}
	if c.TLS.KeyPath != "" {
		klog.Infof("Configured SSL key: %s", c.TLS.KeyPath)
	}
	if c.TLS.CertPath != "" && c.TLS.KeyPath != "" {
		klog.Info("SSL certificate & private key are setup, will try to enable SSL support.")
	}
	klog.Infof("Configured mail backend URL: %s", c.MailBackend.URL)
	klog.Infof("Configured recipient validator: %s", c.RecipientValidator.GetMethod())

	c.RecipientValidator.Print()

	klog.Info("Configured timeouts:")
	klog.Infof("* read timeout: %d seconds", c.ReadTimeoutSeconds)
	klog.Infof("* write timeout: %d seconds", c.WriteTimeoutSeconds)
}

func (c *ConfigImpl) GetHttpListenAddr() string {
	return fmt.Sprintf(":%d", c.HTTP.Port)
}

func (c *ConfigImpl) GetHostname() string {
	return c.Hostname
}

func (c *ConfigImpl) Receiver() ReceiverConfig {
	return c.receiverConfig
}

func (c *ConfigImpl) GetMailBackend() MailBackend {
	return &c.MailBackend
}

func (c *ConfigImpl) GetRecipientValidator() recipientvalidator.Config {
	return c.RecipientValidator
}
