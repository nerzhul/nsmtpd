package config

type ConfigMailBackend struct {
	URL      string
	Username string
	Password string
}

func (c *ConfigMailBackend) GetURL() string {
	return c.URL
}

func (c *ConfigMailBackend) GetUsername() string {
	return c.Username
}

func (c *ConfigMailBackend) GetPassword() string {
	return c.Password
}