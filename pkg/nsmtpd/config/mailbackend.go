package config

type MailBackend interface {
	GetURL() string
	GetUsername() string
	GetPassword() string
}
