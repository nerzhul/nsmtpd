package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfigFilterBackend_Name(t *testing.T) {
	c := &ConfigFilterBackend{}
	assert.Equal(t, "ConfigFilterBackend", c.Name())
}

func TestConfigFilterBackend_Build(t *testing.T) {
	// Default config has 3 enabled backends
	{
		c := &ConfigFilterBackend{}
		cr := NewReceiverConfigRestrictions()
		c.Build(cr)

		assert.Equal(t, 3, c.FilterCount())
	}

	{
		c := &ConfigFilterBackend{}
		cr := NewReceiverConfigRestrictions()
		cr.RestrictedHeaderReplyTo = []string{".+@yopmail.com"}
		c.Build(cr)

		assert.Equal(t, 4, c.FilterCount())
	}
}