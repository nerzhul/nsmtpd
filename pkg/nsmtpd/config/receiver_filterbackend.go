package config

import (
	"net/mail"

	"github.com/go-logr/logr"
	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/receiver/filters"
)

type ConfigFilterBackend struct {
	onDataCommandFilters []filters.Filter
}

func (c *ConfigFilterBackend) Name() string {
	return "ConfigFilterBackend"
}

func (c *ConfigFilterBackend) Build(cr *ReceiverConfigRestrictions) {
	if len(cr.RestrictedKeywords) > 0 {
		c.onDataCommandFilters = append(c.onDataCommandFilters, filters.NewBodyFilter(cr.RestrictedKeywords))
	}

	if len(cr.RestrictedHeaderReplyTo) > 0 {
		c.onDataCommandFilters = append(c.onDataCommandFilters,
			filters.NewHeaderContentFilter("Reply-To", cr.RestrictedHeaderReplyTo))
	}

	if len(cr.RestrictedHeaderTo) > 0 {
		c.onDataCommandFilters = append(c.onDataCommandFilters,
			filters.NewHeaderContentFilter("To", cr.RestrictedHeaderTo))
	}

	if len(cr.MandatoryHeaders) > 0 {
		c.onDataCommandFilters = append(c.onDataCommandFilters, filters.NewMandatoryHeaderFilter(cr.MandatoryHeaders))
	}

	if len(cr.ForbiddenHeaders) > 0 {
		c.onDataCommandFilters = append(c.onDataCommandFilters, filters.NewForbiddenHeaderFilter(cr.ForbiddenHeaders))
	}

	if len(cr.DestDomainWhitelist) > 0 {
		c.onDataCommandFilters = append(c.onDataCommandFilters, filters.NewDestinationDomainWhitelist(cr.DestDomainWhitelist))
	}
}

func (c *ConfigFilterBackend) ApplyOn_DataCommand(m *mail.Message, logger logr.Logger) error {
	for _, filter := range c.onDataCommandFilters {
		if err := filter.Apply(m, logger); err != nil {
			logger.Error(err, "data command failed (filter-check failed - filter=%s)", filter.Name())
			return err
		}
	}

	return nil
}

func (c *ConfigFilterBackend) FilterCount() int {
	return len(c.onDataCommandFilters)
}
