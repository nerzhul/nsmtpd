package config

import "gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/receiver/filters"

type ReceiverConfig interface {
	Name() string
	Load() error
	IsMXValidationEnabled() bool
	IsSPFValidationEnabled() bool
	IsDKIMValidationEnabled() bool
	IsRestrictedFromDomain(domain []byte) (bool, error)
	GetDNSBLMinHits() int
	GetDNSBLCacheTTL() int
	GetFilteringBackend() filters.FilterBackend
	Print()
}

type ReceiverConfigRestrictions struct {
	MXValidation            bool
	DNSBLMinHits            int
	DNSBLCacheTTL           int
	SPFValidation           bool
	DKIMValidation          bool
	RestrictedKeywords      []string `envconfig:"RESTRICTIONS_RESTRICTED_KEYWORDS"`
	RestrictedHeaderReplyTo []string `envconfig:"RESTRICTIONS_RESTRICTED_REPLYTO"`
	RestrictedHeaderTo      []string `envconfig:"RESTRICTIONS_RESTRICTED_HEADER_TO"`
	DestDomainWhitelist     []string `envconfig:"RESTRICTIONS_DEST_DOMAIN_WHITELIST"`
	FromDomainBlacklist     []string `envconfig:"RESTRICTIONS_BLACKLIST_MAILFROM_DOMAINS"`
	MandatoryHeaders        []string `envconfig:"RESTRICTIONS_MANDATORY_HEADERS"`
	ForbiddenHeaders        []string `envconfig:"RESTRICTIONS_FORBIDDEN_HEADERS"`
}

func NewReceiverConfigRestrictions() *ReceiverConfigRestrictions {
	return &ReceiverConfigRestrictions{
		MXValidation:            true,
		DNSBLMinHits:            1,
		SPFValidation:           true,
		DKIMValidation:          false,
		DNSBLCacheTTL:           900,
		FromDomainBlacklist:     []string{},
		RestrictedKeywords:      []string{},
		RestrictedHeaderReplyTo: []string{},
		MandatoryHeaders:        []string{"To", "From", "Subject"},
		ForbiddenHeaders:        []string{"X-CampaignID"},
		RestrictedHeaderTo:      []string{},
		DestDomainWhitelist:     []string{"^.+$"},
	}
}
