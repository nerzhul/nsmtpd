package config

import "gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/recipientvalidator"

type Config interface {
	GetHostname() string
	GetMailBackend() MailBackend
	Receiver() ReceiverConfig
	GetRecipientValidator() recipientvalidator.Config
}
