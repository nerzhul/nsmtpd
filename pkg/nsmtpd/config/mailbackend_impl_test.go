package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMailBackend(t *testing.T) {
	c := &ConfigMailBackend{
		URL:      "smtp://localhost:25",
		Username: "user",
		Password: "password",
	}

	assert.Equal(t, c.GetURL(), "smtp://localhost:25")
	assert.Equal(t, c.GetUsername(), "user")
	assert.Equal(t, c.GetPassword(), "password")
}
