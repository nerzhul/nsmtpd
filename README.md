# nsmtpd & nimapd

_nrz smtpd_ and _nrz imapd_ formelly called _nsmtpd_ and _nimapd_ are 2 single binary softwares to host
mailing reception on your own easily.

# Dependencies

_nsmtpd_ can run standalone, but depends on _nimapd_ for authentication, whereas it can store mails in any
`lmtp` compatible backend.

_nimapd_ relies on a [PostgreSQL](https://www.postgresql.org) database to store mails.

# Security

## TLS

Whereas it's possible to run without any TLS, it's recommended to provide tls key and certificates.

If you provide this couple, _nsmtpd_ and _nimapd_ will run in TLS mode only. Nowadays it's extremely rare to
use clients without TLS.

## Authentication

_nsmtpd_ depend on _nimapd_ for authentication.

_nimapd_ support 2 authentication model for now, but more can be easily added implementing UserValidator
and UserBackend interfaces.

* Database (PostgreSQL only)
* LDAP (tested on OpenLDAP)

## Mail transit between nsmtpd and nimapd

Mail transit between components is done over HTTP. it's recommended to enable HTTPS if transit is done on an external machine.

# Configuration

All configuration is performed using environment variables.

## nsmtpd

* `RECEIVERPORT`: Port to receive messages. It's recommended to do a proper port mapping to run service as non-root. Example: `10025`.
* `RELAYPORT`: Port to receive mails as a relay. Example: `45587`.
* `HOSTNAME`: Hostname provided by the SMTP protocol to clients. example: `smtp.example.org`.
* `MAXMESSAGESIZEMB`: Max message size in megabytes. Example: `10`.
* `MAXRECIPIENTS`: Max recipients in mail headers. Example: `50`.
* `MAILBACKEND_URL`: nimapd backend URL. Example: `http://nimapd:8080`
* `MAILBACKEND_PASSWORD`: nimapd password to transfer mails.
* `MAILBACKEND_USERNAME`: nimapd username to transfer mails.
* `RECIPIENTVALIDATOR_METHOD`: recipient validation method. Values: `http`, `ldap`.
* `RECIPIENTVALIDATOR_ENCRYPTKEY`: encryption key used to AES encrypt fields when validating with backend.
* `RECIPIENTVALIDATOR_URL`: recipient validation URL. Example: `http://nimapd:8080/v1/mail/rcpt-check`
* `RECIPIENTVALIDATOR_USER`: recipient validation username authenticating against backend.
* `RECIPIENTVALIDATOR_PASSWORD`: recipient validation password authenticating against backend.
* `DYNAMICALIASING_ENABLE`: enable dynamic mail aliasing. Example: `true`.
* `DYNAMICALIASING_SEPARATOR`: separator to use for dynamic mail aliasing. Example: `+`
* `RESTRICTIONS_DNSBLMINHITS`: number of DNS RBL to hit to discard mail: Example: `5`.
* `RESTRICTIONS_MXVALIDATION`: enable MX validation for incoming mail.
* `RESTRICTIONS_DKIMVALIDATION`: enable DKIM validation for incoming mail (experimental).
* `RESTRICTIONS_SPFVALIDATION`: enable SPF validation for incoming mail.
* `RESTRICTIONS_BLACKLIST_MAILFROM_DOMAINS`: comma separated list of domains golang formated regex to blacklist. Example: `example.com,example.org,.*pouet.org`.
* `TLS_CERTPATH`: TLS certificate for SMTP protocol. Example: `/etc/ssl/certificates/ssl.crt`
* `TLS_KEYPATH`: TLS privat key for SMTP protocol. Example: `/etc/ssl/certificates/ssl.key`
