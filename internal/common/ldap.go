package common

import (
	"errors"
	"fmt"

	ldap "github.com/go-ldap/ldap/v3"
	"k8s.io/klog/v2"
)

type LDAPConfig struct {
	// LDAP
	URL          string
	UserPattern  string
	BaseDN       string
	BindDN       string
	BindPassword string
	DomainFilter string
	DomainAttr   string
	AliasFilter  string
	AliasAttr    string
	UserNameAttr string
	UserFilter   string
}

type LDAPConn struct {
	config *LDAPConfig
	conn   *ldap.Conn
	logged bool
}

func NewLDAPConn(c *LDAPConfig) *LDAPConn {
	return &LDAPConn{
		config: c,
		logged: false,
	}
}

func (l *LDAPConn) Connect() error {
	conn, err := ldap.Dial("tcp", l.config.URL)
	if err != nil {
		return errors.New(fmt.Sprintf("Unable to connect to LDAP server %s: %v", l.config.URL, err))
	}

	l.conn = conn

	return nil
}

func (l *LDAPConn) Login() error {
	l.logged = false

	err := l.conn.Bind(l.config.BindDN, l.config.BindPassword)
	if err != nil {
		return errors.New(
			fmt.Sprintf("Unable to bind to %s as %s: %v", l.config.URL, l.config.BindDN, err),
		)
	}

	l.logged = true
	return nil
}

func (l *LDAPConn) VerifyUserAuth(username, password string) error {
	ldapRealUsername := fmt.Sprintf(l.config.UserPattern, username)
	err := l.conn.Bind(ldapRealUsername, password)
	if err != nil {
		return errors.New(fmt.Sprintf("Unable to authenticate to %s as %s: %v", l.config.URL, username, err))
	}

	return nil
}

func (l *LDAPConn) GetUsernameFromMail(mailAddr string) (string, error) {
	if !l.logged {
		klog.Fatal("Method called while not authed to LDAP. Aborting.")
	}

	res, err := l.conn.Search(&ldap.SearchRequest{
		BaseDN:       l.config.BaseDN,
		Scope:        ldap.ScopeWholeSubtree,
		DerefAliases: ldap.NeverDerefAliases,
		SizeLimit:    0,
		TimeLimit:    0,
		TypesOnly:    false,
		Filter:       fmt.Sprintf(l.config.AliasFilter, mailAddr),
		Attributes:   []string{l.config.AliasAttr},
		Controls:     nil,
	})

	if err != nil {
		return "", err
	}

	if len(res.Entries) == 0 || len(res.Entries[0].Attributes) == 0 || len(res.Entries[0].Attributes[0].Values) == 0 {
		return "", nil
	}

	return res.Entries[0].Attributes[0].Values[0], nil
}

func (l *LDAPConn) UserExists(username string) (bool, error) {
	if !l.logged {
		klog.Fatal("Method called while not authed to LDAP. Aborting.")
	}

	res, err := l.conn.Search(&ldap.SearchRequest{
		BaseDN:       l.config.BaseDN,
		Scope:        ldap.ScopeWholeSubtree,
		DerefAliases: ldap.NeverDerefAliases,
		SizeLimit:    0,
		TimeLimit:    0,
		TypesOnly:    false,
		Filter:       fmt.Sprintf(l.config.UserFilter, username),
		Attributes:   []string{l.config.UserNameAttr},
		Controls:     nil,
	})

	if err != nil {
		return false, err
	}

	if len(res.Entries) == 0 || len(res.Entries[0].Attributes) == 0 || len(res.Entries[0].Attributes[0].Values) == 0 {
		return false, nil
	}

	return true, nil
}

func (l *LDAPConn) Close() {
	if l.conn != nil {
		l.conn.Close()
	}
}
