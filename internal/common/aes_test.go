package common

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAESEncryptDecrypt(t *testing.T) {
	testKey := []byte("ci4uxu2Ziph0Uuhezaex1chul8keiTai")
	res, err := AESEncryptToBase64(testKey, []byte("this is a test"))
	assert.NoError(t, err)

	res, err = AESDecrypt(testKey, []byte(res))
	assert.NoError(t, err)
	assert.Equal(t, "this is a test", res)
}
