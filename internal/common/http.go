package common

type HTTPConfig struct {
	Port         int
	AuthUser     string
	AuthPassword string
	EncryptKey   string
}

type ImapRelayMailBody struct {
	MailContent string `json:"mail_content"`
}