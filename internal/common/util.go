package common

import (
	"fmt"
	"net/mail"
	"strings"
)

// DynamicAliasingConfig common config for dynamic aliasing
type DynamicAliasingConfig struct {
	Enable    bool `envconfig:"DYNAMICALIASING_ENABLE"`
	Separator string `envconfig:"DYNAMICALIASING_SEPARATOR"`
}

// ResolveDynamicAliasedMail return a new mail.Address if aliasing is enabled and valide
// or the original address
func ResolveDynamicAliasedMail(orig *mail.Address, enabled bool, delim rune) (*mail.Address, error) {
	if !enabled {
		return orig, nil
	}

	delimPos := strings.IndexRune(orig.Address, delim)
	if delimPos < 0 {
		return orig, nil
	}

	atPos := strings.IndexAny(orig.Address, "@")
	if atPos < 0 {
		return orig, fmt.Errorf("invalid address, no @ found")
	}

	realAddr := &mail.Address{
		Name:    orig.Name,
		Address: orig.Address[:delimPos] + orig.Address[atPos:],
	}

	return realAddr, nil
}
