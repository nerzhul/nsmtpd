package common

import (
	"gotest.tools/v3/assert"
	"net/mail"
	"testing"
)

func TestResolveDynamicAliasedMail(t *testing.T) {
	// Addr with no alias tests
	maddr, err := mail.ParseAddress("John Doe <john.doe@example.org>")
	assert.NilError(t, err)
	assert.Equal(t, maddr.Name, "John Doe")
	assert.Equal(t, maddr.Address, "john.doe@example.org")

	{
		r, err := ResolveDynamicAliasedMail(maddr, false, '+')
		assert.NilError(t, err)
		assert.Equal(t, maddr.Name, r.Name)
		assert.Equal(t, maddr.Address, r.Address)
	}

	{
		r, err := ResolveDynamicAliasedMail(maddr, false, 'd')
		assert.NilError(t, err)
		assert.Equal(t, maddr.Name, r.Name)
		assert.Equal(t, maddr.Address, r.Address)
	}

	{
		r, err := ResolveDynamicAliasedMail(maddr, true, '+')
		assert.NilError(t, err)
		assert.Equal(t, maddr.Name, r.Name)
		assert.Equal(t, maddr.Address, r.Address)
	}

	// Aliased tests
	maddr, err = mail.ParseAddress("John Doe <john.doe+testme@example.org>")
	assert.NilError(t, err)
	assert.Equal(t, maddr.Name, "John Doe")
	assert.Equal(t, maddr.Address, "john.doe+testme@example.org")

	// Empty alias delimiter

	// Real delimiter
	{
		r, err := ResolveDynamicAliasedMail(maddr, true, '+')
		assert.NilError(t, err)
		assert.Equal(t, maddr.Name, r.Name)
		assert.Equal(t, "john.doe@example.org", r.Address)
	}

	// Wrong delimiter
	{
		r, err := ResolveDynamicAliasedMail(maddr, true, '_')
		assert.NilError(t, err)
		assert.Equal(t, maddr.Name, r.Name)
		assert.Equal(t, maddr.Address, r.Address)
	}
}
