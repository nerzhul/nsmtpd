package relay

import (
	"log"
	"time"

	"github.com/emersion/go-smtp"
	"gitlab.com/nerzhul/nsmtpd/internal/common"
	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/config"
)

func New(c *config.ConfigImpl) *smtp.Server {
	be := &Backend{
		config: c,
	}

	s := smtp.NewServer(be)

	s.Addr = c.GetRelayListeningAddr()
	s.Domain = c.Hostname
	s.ReadTimeout = 10 * time.Second
	s.WriteTimeout = 10 * time.Second
	s.MaxMessageBytes = int64(c.MaxMessageSizeMB * 1024 * 1024)
	s.MaxRecipients = c.MaxRecipients
	s.AllowInsecureAuth = false
	s.TLSConfig = nil

	if c.TLS.CertPath != "" && c.TLS.KeyPath != "" {
		tlsConfig, err := common.LoadX509KeyPair(c.TLS.CertPath, c.TLS.KeyPath)
		if err != nil {
			log.Fatalf("Unable to load X509 key pair for relay: %s", err)
		}
		s.TLSConfig = tlsConfig
	}

	return s
}
