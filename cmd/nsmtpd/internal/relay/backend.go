package relay

import (
	"github.com/emersion/go-smtp"
	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/config"
	"k8s.io/klog/v2"
)

type Backend struct {
	config *config.ConfigImpl
}

func (b *Backend) NewSession(c *smtp.Conn) (smtp.Session, error) {
	klog.InfoS("New session", "remote-addr", c.Conn().RemoteAddr(), "smtp-hostname", c.Hostname())
	return newSession(c.Conn().RemoteAddr().String(), &b.config.LDAP), nil
}
