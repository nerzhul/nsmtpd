package relay

import (
	"fmt"
	"io"
	"gitlab.com/nerzhul/nsmtpd/internal/common"
	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/codes"

	"github.com/emersion/go-smtp"
	"k8s.io/klog/v2"
)

// A Session is returned after successful login.
type Session struct {
	remoteAddr string
	ldapConfig *common.LDAPConfig
	authed     bool
}

func newSession(remoteAddr string, ldapConfig *common.LDAPConfig) *Session {
	return &Session{
		remoteAddr: remoteAddr,
		ldapConfig: ldapConfig,
	}
}

func (s *Session) AuthPlain(username, password string) error {
	conn := common.NewLDAPConn(s.ldapConfig)
	if err := conn.Connect(); err != nil {
		klog.ErrorS(err, "Failed to connect to LDAP server", "remote-addr", s.remoteAddr, "username", username)
		return codes.ErrTempFailUserLookup
	}

	defer conn.Close()

	if err := conn.VerifyUserAuth(username, password); err != nil {
		klog.ErrorS(err, "Failed to verify user auth", "remote-addr", s.remoteAddr, "username", username)
		return smtp.ErrAuthFailed
	}

	s.authed = true
	return nil
}

func (s *Session) Mail(from string, opts *smtp.MailOptions) error {
	if !s.authed {
		klog.ErrorS(fmt.Errorf("unauthed mail command received"), "", "from", from, "remote-addr", s.remoteAddr)
		return smtp.ErrAuthRequired
	}

	klog.InfoS("Mail from", "from", from, "remote-addr", s.remoteAddr)

	return nil
}

func (s *Session) Rcpt(to string, opts *smtp.RcptOptions) error {
	if !s.authed {
		klog.ErrorS(fmt.Errorf("unauthed rcpt command received"), "", "to", to, "remote-addr", s.remoteAddr)
		return smtp.ErrAuthRequired
	}

	klog.InfoS("Rcpt to", "to", to, "remote-addr", s.remoteAddr)

	return nil
}

func (s *Session) Data(r io.Reader) error {
	if !s.authed {
		klog.ErrorS(fmt.Errorf("unauthed data command received"), "remote-addr", s.remoteAddr)
		return smtp.ErrAuthRequired
	}

	if b, err := io.ReadAll(r); err != nil {
		klog.ErrorS(err, "Failed to read data", "remote-addr", s.remoteAddr, "data", string(b))
	} else {
		klog.InfoS("data command received", "remote-addr", s.remoteAddr)
	}
	return nil
}

func (s *Session) Reset() {}

func (s *Session) Logout() error {
	s.authed = false
	klog.InfoS("logout received", "remote-addr", s.remoteAddr)
	return nil
}
