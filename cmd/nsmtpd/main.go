package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/emersion/go-smtp"
	"github.com/fsnotify/fsnotify"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/nerzhul/nsmtpd/cmd/nsmtpd/internal/relay"
	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/config"
	"gitlab.com/nerzhul/nsmtpd/pkg/nsmtpd/receiver"
	"k8s.io/klog/v2"
)

func main() {
	c := &config.ConfigImpl{}

	c.Load()

	http.Handle("/metrics", promhttp.Handler())

	stopSigs := make(chan os.Signal, 1)
	signal.Notify(stopSigs, syscall.SIGINT, syscall.SIGTERM)
	stop := make(chan bool, 1)

	reloadSigs := make(chan os.Signal, 1)
	signal.Notify(reloadSigs, syscall.SIGHUP)

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		klog.Fatalf("Unable to create fsnotify watcher: %s", err)
	}
	defer watcher.Close()

	recv := startReceiver(c)
	relay := startRelay(c)

	// Reload/Stop handler
	go func() {
		for {
			select {
			case sig := <-stopSigs:
				{
					klog.Infof("Received signal %s, stopping services", sig)
					stop <- true
					recv.Shutdown(context.TODO())
					relay.Shutdown(context.TODO())
				}
			case sig := <-reloadSigs:
				{
					klog.Infof("Received signal %s, restarting listeners configuration", sig)
					if err := recv.Shutdown(context.TODO()); err != nil {
						klog.Errorf("Unable to shutdown receiver: %s", err)
					}
					recv = startReceiver(c)

					if err := relay.Shutdown(context.TODO()); err != nil {
						klog.Errorf("Unable to shutdown relay: %s", err)
					}

					relay = startRelay(c)
				}
			case event := <-watcher.Events:
				{
					// only handle writes
					if !event.Has(fsnotify.Write) {
						continue
					}

					klog.Infof("Watched file %s modified, restarting listeners configuration", event.Name)
					if err := recv.Shutdown(context.TODO()); err != nil {
						klog.Errorf("Unable to shutdown receiver: %s", err)
					}
					recv = startReceiver(c)

					if err := relay.Shutdown(context.TODO()); err != nil {
						klog.Errorf("Unable to shutdown relay: %s", err)
					}

					relay = startRelay(c)
				}
			}

		}
	}()

	if c.TLS.CertPath != "" && c.TLS.KeyPath != "" {
		klog.InfoS("Watching TLS certificate/key files", "cert", c.TLS.CertPath, "key", c.TLS.KeyPath)
		watcher.Add(c.TLS.CertPath)
		watcher.Add(c.TLS.KeyPath)
	}

	klog.Infof("Starting metrics on port %d", c.HTTP.Port)
	if err := http.ListenAndServe(c.GetHttpListenAddr(), nil); err != nil {
		klog.Fatalf("Unable to startup metrics listener: %s", err)
	}
}

func startReceiver(c *config.ConfigImpl) *smtp.Server {
	recv := receiver.New(c)

	go func() {
		klog.Infof("Starting receiver on port %d", c.ReceiverPort)
		if err := recv.ListenAndServe(); err != nil {
			klog.Fatalf("Unable to listen on receiver: %s", err)
		}
	}()

	return recv
}

func startRelay(c *config.ConfigImpl) *smtp.Server {
	rly := relay.New(c)

	go func() {
		klog.Infof("Starting relay on port %d", c.RelayPort)
		if err := rly.ListenAndServe(); err != nil {
			klog.Fatalf("Unable to listen on relay: %s", err)
		}
	}()

	return rly
}
