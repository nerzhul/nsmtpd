CREATE TABLE user_mail_addresses (
    user_id uuid NOT NULL REFERENCES users(id),
    user_address TEXT NOT NULL UNIQUE,
    PRIMARY KEY (user_id, user_address)
);