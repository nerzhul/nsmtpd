CREATE TYPE mailbox_route_type AS ENUM ('addr_from', 'addr_to');

CREATE TABLE user_mailbox_route (
    user_id uuid NOT NULL REFERENCES users(id),
    route_type mailbox_route_type NOT NULL,
    condition TEXT NOT NULL,
    mailbox_route TEXT NOT NULL,
    PRIMARY KEY (user_id, route_type, condition, mailbox_route)
);