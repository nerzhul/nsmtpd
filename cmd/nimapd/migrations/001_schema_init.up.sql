CREATE EXTENSION "uuid-ossp";
CREATE TABLE users (
    id uuid,
    username TEXT UNIQUE,
    password TEXT NOT NULL DEFAULT '',
    salt TEXT NOT NULL DEFAULT '',
    PRIMARY KEY (id)
);