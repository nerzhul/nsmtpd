CREATE TABLE mails (
    id BIGSERIAL,
    date TIMESTAMP NOT NULL DEFAULT NOW(),
    body TEXT NOT NULL,
    mailbox_id BIGINT NOT NULL REFERENCES user_mailboxes(id) ON DELETE CASCADE,
    PRIMARY KEY (id)
);

CREATE INDEX mails_id_mbid ON mails (mailbox_id, id);

CREATE TABLE mail_flags (
    mail_id BIGINT,
    flag TEXT NOT NULL,
    FOREIGN KEY (mail_id) REFERENCES mails(id) ON DELETE CASCADE,
    PRIMARY KEY (mail_id, flag)
);