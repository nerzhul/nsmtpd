CREATE TABLE user_mailboxes (
    id BIGSERIAL,
    name TEXT,
    user_id uuid REFERENCES users(id),
    subscribed bool,
    PRIMARY KEY (name, user_id)
);

CREATE UNIQUE INDEX user_mailboxes_id ON user_mailboxes(id);
CREATE UNIQUE INDEX user_mailboxes_name_id ON user_mailboxes(user_id, name);
CREATE INDEX user_mailboxes_subscribed ON user_mailboxes(user_id, subscribed);

CREATE TABLE user_mailbox_attributes (
    mailbox_id BIGINT NOT NULL,
	attr TEXT,
    FOREIGN KEY (mailbox_id) REFERENCES user_mailboxes(id) ON DELETE CASCADE,
    PRIMARY KEY (mailbox_id, attr)
);