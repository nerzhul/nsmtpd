package main

import (
	compress "github.com/emersion/go-imap-compress"
	id "github.com/emersion/go-imap-id"
	"github.com/emersion/go-imap/server"
	"k8s.io/klog/v2"
	"log"
	"gitlab.com/nerzhul/nsmtpd/cmd/nimapd/internal"
	"gitlab.com/nerzhul/nsmtpd/internal/common"
	"os"
)

func main() {
	internal.GConfig.Load()

	if !internal.GDatabase.Init() {
		log.Fatal("Failed to initialize database connector, aborting.")
	}

	// For now it's mostly for troubleshoot purposes
	if os.Getenv("INTEGRITY_CHECK") == "1" {
		err := internal.IntegrityCheckAllMails()
		if err != nil {
			log.Fatalf("Integrity check failed: %s", err)
		}
	}

	var uv internal.UserValidator
	switch internal.GConfig.AuthMethod {
	case internal.AuthMethodDB:
		uv = internal.NewDBUserValidator()
	case internal.AuthMethodLDAP:
		uv = internal.NewLDAPUserValidator()
	}

	go func() {
		mr := internal.NewMailReceiver(uv)
		mr.ListenAndServe()
	}()

	be := internal.NewBackend(uv)

	// Create a new server
	s := server.New(be)
	s.Enable(NewExtension())
	s.Enable(id.NewExtension(map[string]string{
		id.FieldName:        "nimapd",
		id.FieldEnvironment: "production",
		id.FieldSupportURL:  "https://gitlab.com/nerzhul/nsmtpd",
		id.FieldVersion:     "latest",
	}))

	if internal.GConfig.Extensions.Compress {
		klog.Info("IMAP compress extension enabled.")
		s.Enable(compress.NewExtension())
	}

	if internal.GConfig.Debug {
		// For debugging purposes
		s.Debug = os.Stdout
	}

	s.Addr = internal.GConfig.GetListeningAddr()
	s.TLSConfig = nil

	if internal.GConfig.TLSCertPath == "" && internal.GConfig.TLSKeyPath == "" {
		// Since we will use this server for testing only, we can allow plain text
		// authentication over unencrypted connections
		s.AllowInsecureAuth = true

		klog.Infof("Starting IMAP server at %s", internal.GConfig.GetListeningAddr())
		if err := s.ListenAndServe(); err != nil {
			klog.Fatal(err)
		}
	} else {
		s.AllowInsecureAuth = false

		tlsConfig, err := common.LoadX509KeyPair(internal.GConfig.TLSCertPath, internal.GConfig.TLSKeyPath)
		if err != nil {
			log.Fatalf("Unable to load X509 key pair on TLS support enable: %s", err)
		}
		s.TLSConfig = tlsConfig

		klog.Infof("Starting IMAP server at %s (tls)", internal.GConfig.GetListeningAddr())
		if err := s.ListenAndServeTLS(); err != nil {
			klog.Fatal(err)
		}
	}
}

type extension struct{}

func (e extension) Capabilities(c server.Conn) []string {
	return []string{}
}

func (e extension) Command(name string) server.HandlerFactory {
	if name != "LIST" {
		return nil
	}

	return func() server.Handler { return &internal.ListCommandOverwrite{} }
}

func NewExtension() server.Extension {
	return &extension{}
}
