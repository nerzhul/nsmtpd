package internal

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"net/mail"
	"gitlab.com/nerzhul/nsmtpd/internal/common"
	"strings"
	"time"

	"github.com/emersion/go-imap"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"k8s.io/klog/v2"
)

const (
	healthCheckPath = "/v1/health"
	metricsPath     = "/v1/metrics"
	deliverPath     = "/v1/mail/deliver/:userList"
	authPath        = "/v1/auth"
	rcptCheckPath   = "/v1/mail/rcpt-check"
)

type MailHTTP struct {
	userValidator      UserValidator
	addrParser         mail.AddressParser
	requestCounter     *prometheus.CounterVec
	requestTimeCounter *prometheus.CounterVec
	userBackend        UserBackend
}

func NewMailReceiver(uv UserValidator) *MailHTTP {
	initMetrics()

	receiver := &MailHTTP{
		userValidator: uv,
		addrParser:    mail.AddressParser{},
		requestCounter: promauto.NewCounterVec(prometheus.CounterOpts{
			Name: "nimapd_mailreceiver_requests_total",
			Help: "mail receiver request count",
		},
			[]string{"request"}),
		requestTimeCounter: promauto.NewCounterVec(prometheus.CounterOpts{
			Name: "nimapd_mailreceiver_request_total_time",
			Help: "Total time spent in requests",
		},
			[]string{"request"}),
	}

	switch GConfig.AuthMethod {
	case AuthMethodDB:
		receiver.userBackend = newUserDBBackend()
	case AuthMethodLDAP:
		receiver.userBackend = common.NewLDAPConn(&GConfig.LDAP)
	default:
		klog.Fatalf("unknown auth method: %s", GConfig.AuthMethod)
	}
	return receiver
}

func (m *MailHTTP) ListenAndServe() {
	e := echo.New()
	middleware.DefaultLoggerConfig.Format = `I0000 ${time_rfc3339_nano} "${id}" "${remote_ip}" "${host}" ${method} ` +
		`${uri} "${user_agent}" ${status} "${error}" ${latency} "${latency_human}" ${bytes_in} ${bytes_out}` + "\n"
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	middleware.DefaultBasicAuthConfig.Validator = m.authValidator
	middleware.DefaultBasicAuthConfig.Skipper = m.skipAuthCallback
	e.Use(middleware.BasicAuthWithConfig(middleware.DefaultBasicAuthConfig))

	// Routes
	e.GET(healthCheckPath, m.health)
	e.POST(deliverPath, m.mailDeliver)
	e.POST(authPath, m.delegAuth)
	e.POST(rcptCheckPath, m.delegRcptCheck)
	e.GET(metricsPath, echo.WrapHandler(promhttp.Handler()))

	// Start server
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", GConfig.HTTP.Port)))
}

type healthcheckResult struct {
	Status string `json:"status"`
}

func (m *MailHTTP) health(c echo.Context) error {
	m.requestCounter.WithLabelValues(healthCheckPath).Inc()
	startTime := time.Now().Nanosecond()
	defer m.requestTimeCounter.WithLabelValues(healthCheckPath).Add(float64(time.Now().Nanosecond()) - float64(startTime))

	return c.JSON(http.StatusOK, &healthcheckResult{Status: "ok"})
}

func (m *MailHTTP) mailDeliver(c echo.Context) error {
	m.requestCounter.WithLabelValues(deliverPath).Inc()
	startTime := time.Now().Nanosecond()
	defer m.requestTimeCounter.WithLabelValues(deliverPath).Add(float64(time.Now().Nanosecond()) - float64(startTime))

	userList := c.Param("userList")
	mailBody := common.ImapRelayMailBody{}
	if err := (&echo.DefaultBinder{}).BindBody(c, &mailBody); err != nil {
		return c.String(http.StatusBadRequest, "invalid request (bind failed)")
	}

	// Verify parameters are set
	if len(userList) == 0 {
		klog.Errorf("unable to process mail delivery HTTP request, some parameters are empty "+
			"(userList %db)", len(userList))
		return c.String(http.StatusBadRequest, "invalid request")
	}

	maddrs, err := m.addrParser.ParseList(userList)
	if err != nil {
		klog.Errorf("unable to parse address list: %s", err.Error())
		return c.String(http.StatusBadRequest, "invalid request")
	}

	if err := m.userBackend.Connect(); err != nil {
		klog.Errorf("unable to connect to LDAP server: %s", err.Error())
		return c.String(http.StatusInternalServerError, "internal server error")
	}

	defer m.userBackend.Close()

	if err := m.userBackend.Login(); err != nil {
		klog.Errorf("unable to login to LDAP server: %s", err.Error())
		return c.String(http.StatusInternalServerError, "internal server error")
	}

	var userListReal []string

	for _, maddr := range maddrs {
		maddrResolved, err := common.ResolveDynamicAliasedMail(maddr, GConfig.DynamicAliasing.Enable, rune(GConfig.DynamicAliasing.Separator[0]))
		if err != nil {
			klog.Errorf("Unable to resolve dynamic aliased mail for mail %s: %s", maddr, err)
			return c.String(http.StatusInternalServerError, "unable to check username aliases")
		}

		username, err := m.userBackend.GetUsernameFromMail(maddrResolved.Address)
		if err != nil {
			klog.Errorf("Unable to fetch username for mail in list %s: %s", maddr, err)
			return c.String(http.StatusInternalServerError, "internal server error")
		}

		if username == "" {
			klog.Warningf("invalid user for address %s. Dropping transaction", maddr)
			return c.String(http.StatusNotFound, fmt.Sprintf("user not found '%s'", maddr))
		}

		klog.Infof("Found user=%s for maddr=%s", username, maddr.String())
		userListReal = append(userListReal, username)
	}

	body, err := base64.StdEncoding.DecodeString(mailBody.MailContent)
	if err != nil {
		klog.Errorf("unable to decode body: %s", err)
		return c.String(http.StatusBadRequest, "invalid body field")
	}

	msg, err := mail.ReadMessage(strings.NewReader(string(body)))
	if err != nil {
		klog.Errorf("Unable to read message: %s", err)
		return c.String(http.StatusInternalServerError, "mail read decoding failure")
	}

	var mbList []*Mailbox
	for _, username := range userListReal {
		found, err := m.userValidator.Exists(username)
		if err != nil {
			klog.Error(err)
			return c.String(http.StatusInternalServerError, "internal server error")
		}

		if !found {
			klog.Errorf("user '%s' not found in user referential", username)
			return c.String(http.StatusNotFound, fmt.Sprintf("user '%s' not found in referentiel", username))
		}

		uid, err := GDatabase.GetUserID(username)
		if err != nil {
			klog.Error(err)
			return c.String(http.StatusNotFound, fmt.Sprintf("user '%s' not found in local db", username))
		}

		route := imap.InboxName

		// First search route depending on source
		if addrs, err := msg.Header.AddressList("From"); err == nil {
			for _, addr := range addrs {
				if newRoute, err := GDatabase.getMailboxAddressRoute(uid, strings.ToLower(addr.Address), "addr_from"); err != nil {
					klog.Errorf("unable to retrieve mailbox address route: %s", err)
				} else if newRoute != "" {
					route = newRoute
				}
			}
		} else {
			klog.Errorf("unable to read 'From' field in mail body")
		}

		// Second search route depending on To field
		if addrs, err := msg.Header.AddressList("To"); err == nil {
			for _, addr := range addrs {
				if newRoute, err := GDatabase.getMailboxAddressRoute(uid, strings.ToLower(addr.Address), "addr_to"); err != nil {
					klog.Errorf("unable to retrieve mailbox address route: %s", err)
				} else if newRoute != "" {
					route = newRoute
				}
			}
		} else {
			klog.Errorf("unable to read 'From' field in mail body")
		}

		mb := NewMailbox(uid, nil)
		err = GDatabase.getMailbox(uid, route, mb)
		if err != nil {
			klog.Errorf("unable to get mailbox %s for user %s: %s", route, uid, err)
			return c.String(http.StatusInternalServerError, "internal server error")
		}

		mbList = append(mbList, mb)
	}

	msgLen := len(body)
	for _, mb := range mbList {
		// Seems to be incomplete currently on the body part, be warned
		if err := GDatabase.CreateMessage(mb.id, []string{imap.RecentFlag}, time.Now(), body); err != nil {
			klog.Errorf("unable to create message to db: %s", err)
			return c.String(http.StatusBadGateway, "unable to save received message")
		}

		klog.Infof("stored mail for user=%s mb=%d, size=%d", mb.owner, mb.id, msgLen)
	}

	return c.String(http.StatusOK, "ok")
}

type authRequest struct {
	Username string `json:"username" query:"username"`
	Password string `json:"password" query:"password"`
}

func (m *MailHTTP) delegAuth(c echo.Context) error {
	m.requestCounter.WithLabelValues(authPath).Inc()
	startTime := time.Now().Nanosecond()
	defer m.requestTimeCounter.WithLabelValues(authPath).Add(float64(time.Now().Nanosecond()) - float64(startTime))

	if len(GConfig.HTTP.EncryptKey) == 0 {
		return c.String(http.StatusNotFound, "not found")
	}

	ar := authRequest{}
	if err := c.Bind(&ar); err != nil {
		return err
	}

	var err error
	ar.Username, err = common.AESDecrypt([]byte(GConfig.HTTP.EncryptKey), []byte(ar.Username))
	if err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	ar.Password, err = common.AESDecrypt([]byte(GConfig.HTTP.EncryptKey), []byte(ar.Password))
	if err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	if err := m.userBackend.Connect(); err != nil {
		klog.Errorf("unable to connect to LDAP server: %s", err.Error())
		return c.String(http.StatusInternalServerError, "internal server error")
	}

	defer m.userBackend.Close()

	if err := m.userBackend.Login(); err != nil {
		klog.Errorf("unable to login to LDAP server: %s", err.Error())
		return c.String(http.StatusInternalServerError, "internal server error")
	}

	if err := m.userBackend.VerifyUserAuth(ar.Username, ar.Password); err != nil {
		return c.String(http.StatusForbidden, "forbidden")
	}

	return c.String(http.StatusOK, "")
}

type delegRcptRequest struct {
	RcptTo string `json:"rcpt-to" query:"rcpt-to"`
}

func (m *MailHTTP) delegRcptCheck(c echo.Context) error {
	m.requestCounter.WithLabelValues(rcptCheckPath).Inc()
	startTime := time.Now().Nanosecond()
	defer m.requestTimeCounter.WithLabelValues(rcptCheckPath).Add(float64(time.Now().Nanosecond()) - float64(startTime))

	if len(GConfig.HTTP.EncryptKey) == 0 {
		return c.String(http.StatusNotImplemented, "not implemented")
	}

	rr := delegRcptRequest{}
	if err := c.Bind(&rr); err != nil {
		return err
	}

	var err error
	rcptToOrig := []byte(rr.RcptTo)
	rr.RcptTo, err = common.AESDecrypt([]byte(GConfig.HTTP.EncryptKey), rcptToOrig)
	if err != nil {
		klog.Errorf("delegated recipient check: unable to decrypt rcpt-to field (len %d). Error was: %s",
			len(rcptToOrig), err)
		return c.String(http.StatusBadRequest, "bad request")
	}

	addrParser := mail.AddressParser{}

	maddrs, err := addrParser.ParseList(rr.RcptTo)
	if err != nil {
		klog.Errorf("delegated recipient check: unable to parse delegated recipients: %s", err.Error())
		return c.String(http.StatusBadRequest, "malformed recipient list")
	}

	if err := m.userBackend.Connect(); err != nil {
		klog.Errorf("delegated recipient check: unable to connect to LDAP server: %s", err.Error())
		return c.String(http.StatusInternalServerError, "internal server error")
	}

	defer m.userBackend.Close()

	if err := m.userBackend.Login(); err != nil {
		klog.Errorf("delegated recipient check: unable to login to LDAP server: %s", err.Error())
		return c.String(http.StatusInternalServerError, "internal server error")
	}

	for _, maddr := range maddrs {
		maddr, err = common.ResolveDynamicAliasedMail(maddr, GConfig.DynamicAliasing.Enable, rune(GConfig.DynamicAliasing.Separator[0]))
		if err != nil {
			klog.Errorf("unable to resolve dynamic aliased mail for mail %s: %s", maddr, err)
			return c.String(http.StatusInternalServerError, "unable to check username aliases")
		}

		klog.InfoS("delegated recipient check: checking recipient", "recipient", maddr)

		username, err := m.userBackend.GetUsernameFromMail(maddr.Address)
		if err != nil {
			klog.Errorf("delegated recipient check: Unable to fetch username for mail list %s: %s", maddr, err)
			return c.String(http.StatusInternalServerError, "unable to check username")
		}

		if username == "" {
			klog.Warningf("delegated recipient check: invalid user %s. Dropping transaction", maddr)
			return c.String(http.StatusNotFound, "user not found")
		}

		klog.InfoS("delegated recipient check: recipient is valid", "recipient", maddr.String(), "mapped-user", username)
	}

	return c.String(http.StatusOK, "")
}

func (m *MailHTTP) authValidator(username string, password string, c echo.Context) (bool, error) {
	if username != GConfig.HTTP.AuthUser || password != GConfig.HTTP.AuthPassword {
		return false, nil
	}

	return true, nil
}

func (m *MailHTTP) skipAuthCallback(c echo.Context) bool {
	return c.Path() == healthCheckPath || c.Path() == metricsPath
}
