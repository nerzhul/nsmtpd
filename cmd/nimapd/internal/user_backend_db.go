package internal

import (
	"errors"
)

type userDBBackend struct{}

func newUserDBBackend() *userDBBackend {
	return &userDBBackend{}
}

// Connect: noop in database context
func (b *userDBBackend) Connect() error {
	// noop
	return nil
}

// Login: noop in database context
func (b *userDBBackend) Login() error {
	// noop
	return nil
}

// Close: noop in database context
func (b *userDBBackend) Close() {}

// VerifyUserAuth: verify user authentication against database
func (b *userDBBackend) VerifyUserAuth(username string, password string) error {
	return errors.New("not implemented")
}

// GetUserFromMail: retrieve user from mail address
func (b *userDBBackend) GetUsernameFromMail(mailAddr string) (string, error) {
	return GDatabase.getUsernameFromMail(mailAddr)
}
