package internal

type UserBackend interface {
	Connect() error
	Login() error
	Close()
	VerifyUserAuth(string, string) error
	GetUsernameFromMail(mailAddr string) (string, error)
}
