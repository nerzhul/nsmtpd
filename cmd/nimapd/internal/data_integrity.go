package internal

import (
	"k8s.io/klog/v2"
)

func IntegrityCheckAllMails() error {
	msg, err := GDatabase.ListAllMessages()
	if err != nil {
		return err
	}

	for _, m := range msg {
		// It's only for very specific troubleshooting, don't enable it onto production
		klog.Infof("Checking mail uid=%d flags=%v", m.Uid, m.Flags)

		// msg, err := mail.ReadMessage(bytes.NewReader(m.Body))
		// if err != nil {
		// 	klog.Errorf("Error reading mail uid=%d: %v", m.Uid, err)
		// 	continue
		// }

		// if strings.Contains(string(m.Body), "MAIL FROM") {
		// 	klog.Infof("Mail uid=%d contains MAIL FROM. From %s subject %s date: %s", m.Uid, msg.Header.Get("From"), msg.Header.Get("Subject"), msg.Header.Get("Date"))
		// 	klog.Infof("Debug mail: %s", string(m.Body))
		// } else {
		// 	klog.Infof("Mail uid=%d from=%s subject=%s date=%s", m.Uid, msg.Header.Get("From"), msg.Header.Get("Subject"), msg.Header.Get("Date"))
		// }
	}

	return nil
}
