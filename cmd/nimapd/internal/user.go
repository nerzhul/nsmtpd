package internal

import (
	"fmt"
	"github.com/emersion/go-imap/backend"
	"k8s.io/klog/v2"
)

type User struct {
	username string
	id       string
	updates  chan backend.Update
}

func NewUser(id string, login string, updates chan backend.Update) *User {
	return &User{
		id:       id,
		username: login,
		updates:  updates,
	}
}

func (u *User) Username() string {
	return u.username
}

func (u *User) ListMailboxes(subscribed bool) ([]backend.Mailbox, error) {
	mbList, err := GDatabase.ListMailboxes(u, subscribed)
	if err != nil {
		klog.Errorf("Unable to ListMailboxes for user %s: %s", u.username, err)
		return nil, err
	}

	// No mailbox, create them
	if len(mbList) == 0 {
		if err := u.createDefaultMailboxes(); err != nil {
			return nil, err
		}

		// we created mailboxes, relist them
		return u.ListMailboxes(subscribed)
	}

	return mbList, nil
}

func (u *User) GetMailbox(name string) (backend.Mailbox, error) {
	mbDest := NewMailbox(u.getID(), u.updates)
	err := GDatabase.getMailbox(u.getID(), name, mbDest)
	if err != nil {
		return nil, err
	}
	return mbDest, nil
}

func (u *User) CreateMailbox(name string) error {
	exists, err := GDatabase.isMailboxExists(u.id, name)
	if err != nil {
		return err
	}

	if exists {
		return fmt.Errorf("mailbox %s already exists", name)
	}

	return GDatabase.createMailbox(u, name)
}

func (u *User) DeleteMailbox(name string) error {
	exists, err := GDatabase.isMailboxExists(u.id, name)
	if err != nil {
		return err
	}

	if !exists {
		return fmt.Errorf("mailbox %s doesn't exists", name)
	}

	return GDatabase.deleteMailbox(u, name)
}

func (u *User) RenameMailbox(existingName, newName string) error {
	exists, err := GDatabase.isMailboxExists(u.id, existingName)
	if err != nil {
		return err
	}

	if !exists {
		return fmt.Errorf("mailbox %s doesn't exists", existingName)
	}

	return GDatabase.renameMailbox(u, existingName, newName)
}

func (u *User) Logout() error {
	// NOOP we are stateless
	return nil
}

func (u *User) getID() string {
	return u.id
}

func (u *User) createDefaultMailboxes() error {
	klog.V(0).Infof("Creating default mailboxes for user %s", u.username)
	for _, mb := range []string{MailboxInbox, MailboxDrafts, MailboxTrash, MailboxSent} {
		err := GDatabase.createMailbox(u, mb)
		if err != nil {
			klog.Errorf("Unable to createDefaultMailbox %s for user %s: %s", mb, u.username, err)
			return err
		}
	}

	return nil
}
