package internal

import (
	"fmt"
	"gitlab.com/nerzhul/nsmtpd/internal/common"

	"k8s.io/klog/v2"
)

type LDAPUserValidator struct {}

func NewLDAPUserValidator() UserValidator {
	return &LDAPUserValidator{}
}

func (u *LDAPUserValidator) Login(username string, password string) (string, error) {
	conn := common.NewLDAPConn(&GConfig.LDAP)
	if err := conn.Connect(); err != nil {
		klog.Errorf(err.Error())
		return "", fmt.Errorf("unable to connect to auth source")
	}

	defer conn.Close()

	if err := conn.VerifyUserAuth(username, password); err != nil {
		klog.Error(err.Error())
		return "", fmt.Errorf("unable to login, username or password incorrect")
	}

	userID, err := GDatabase.GetUserID(username)
	if err != nil {
		klog.Errorf(err.Error())
		return "", fmt.Errorf("unable to login: server error")
	}

	if userID == "" {
		klog.V(0).Infof("user %s not found in database, creating user", username)
		userID, err = GDatabase.RegisterUser(username, "", "")
		if err != nil {
			klog.Errorf("unable to create %s user account: %s", username, err)
			return "", fmt.Errorf("unable to create user account")
		}
	}

	klog.Infof("user %s authed successfully", username)
	return userID, nil
}

func (u *LDAPUserValidator) Exists(username string) (bool, error) {
	conn := common.NewLDAPConn(&GConfig.LDAP)
	if err := conn.Connect(); err != nil {
		klog.Errorf("unable to connect to LDAP server: %s", err)
		return false, err
	}

	defer conn.Close()

	if err := conn.Login(); err != nil {
		klog.Errorf("unable to login to LDAP server: %s", err)
		return false, err
	}

	exists, err := conn.UserExists(username)
	if err != nil {
		klog.Errorf("error while searching username: %s", err)
	}
	return exists, err
}
