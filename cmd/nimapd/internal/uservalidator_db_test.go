package internal

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"golang.org/x/crypto/bcrypt"
)

func TestUserValidatorDB_HashedPassword(t *testing.T) {
	password := "prout"
	salt := "ohloez3Fo8ohdaiquah5oov-a"

	saltedPwd := fmt.Sprintf("%s/%s", password, salt)
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(saltedPwd), bcrypt.DefaultCost)
	assert.Nil(t, err)

	err = bcrypt.CompareHashAndPassword(hashedPassword, []byte(saltedPwd))
	assert.Nil(t, err)
}
