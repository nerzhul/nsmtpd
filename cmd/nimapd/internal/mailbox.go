package internal

import (
	"fmt"
	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/backend"
	"github.com/emersion/go-imap/backend/backendutil"
	"k8s.io/klog/v2"
	"time"
)

type Mailbox struct {
	info    imap.MailboxInfo
	id      int64
	owner   string
	updates chan backend.Update
}

func NewMailbox(username string, updates chan backend.Update) *Mailbox {
	return &Mailbox{
		owner:   username,
		updates: updates,
	}
}

func (m Mailbox) Name() string {
	mailboxMetrics.counterCmd.WithLabelValues("NAME", m.owner).Inc()
	startTime := time.Now().Nanosecond()
	defer mailboxMetrics.counterTimeCmd.WithLabelValues("NAME", m.owner).Add(float64(time.Now().Nanosecond()) - float64(startTime))

	klog.V(2).Infof("obj=mailbox verb=NAME user-id='%s' msg='mailbox name requested'", m.owner)
	return m.info.Name
}

func (m Mailbox) Info() (*imap.MailboxInfo, error) {
	mailboxMetrics.counterCmd.WithLabelValues("INFO", m.owner).Inc()
	startTime := time.Now().Nanosecond()
	defer mailboxMetrics.counterTimeCmd.WithLabelValues("INFO", m.owner).Add(float64(time.Now().Nanosecond()) - float64(startTime))

	klog.V(2).Infof("obj=mailbox verb=INFO user-id='%s' msg='mailbox info requested'", m.owner)
	return &m.info, nil
}

func (m Mailbox) Status(items []imap.StatusItem) (*imap.MailboxStatus, error) {
	mailboxMetrics.counterCmd.WithLabelValues("STATUS", m.owner).Inc()
	startTime := time.Now().Nanosecond()
	defer mailboxMetrics.counterTimeCmd.WithLabelValues("STATUS", m.owner).Add(float64(time.Now().Nanosecond()) - float64(startTime))

	klog.V(2).Infof("obj=mailbox verb=STATUS user-id='%s' msg='mailbox status requested'", m.owner)

	var err error

	status := imap.NewMailboxStatus(m.info.Name, items)
	status.Flags, err = GDatabase.ListMailFlagsForMailbox(m.id)
	if err != nil {
		return nil, err
	}

	status.PermanentFlags = []string{"\\*"}
	status.UnseenSeqNum, err = GDatabase.GetMailboxFirstUnseenSeqNum(m.id)
	if err != nil {
		return nil, err
	}

	for _, name := range items {
		switch name {
		case imap.StatusMessages:
			status.Messages, err = GDatabase.GetMailboxMessageCount(m.id)
			if err != nil {
				return nil, err
			}
		case imap.StatusUidNext:
			status.UidNext, err = GDatabase.GetNextMailUID()
			if err != nil {
				return nil, err
			}
		case imap.StatusUidValidity:
			status.UidValidity = 1
		case imap.StatusRecent:
			status.Unseen, err = GDatabase.GetMailboxMessageCountByFlag(m.id, imap.RecentFlag)
			if err != nil {
				return nil, err
			}
		case imap.StatusUnseen:
			status.Unseen, err = GDatabase.GetMailboxUnseenMessageCount(m.id)
			if err != nil {
				return nil, err
			}
		}
	}

	return status, nil
}

func (m Mailbox) SetSubscribed(subscribed bool) error {
	mailboxMetrics.counterCmd.WithLabelValues("SUBSCRIBE", m.owner).Inc()
	startTime := time.Now().Nanosecond()
	defer mailboxMetrics.counterTimeCmd.WithLabelValues("SUBSCRIBE", m.owner).Add(float64(time.Now().Nanosecond()) - float64(startTime))
	klog.V(2).Infof("obj=mailbox verb=INFO user-id='%s' msg='subscribed to mailbox %s'", m.owner, m.info.Name)

	return GDatabase.setSubscribed(m.id, subscribed)
}

func (m Mailbox) Check() error {
	panic("implement me")
}

func (m Mailbox) ListMessages(uid bool, seqset *imap.SeqSet, items []imap.FetchItem, ch chan<- *imap.Message) error {
	mailboxMetrics.counterCmd.WithLabelValues("LIST", m.owner).Inc()
	startTime := time.Now().Nanosecond()
	defer mailboxMetrics.counterTimeCmd.WithLabelValues("LIST", m.owner).Add(float64(time.Now().Nanosecond()) - float64(startTime))

	klog.V(2).Infof("obj=mailbox verb=LIST user-id='%s' msg='list mailbox %s messages (seqset=%s)'",
		m.owner, m.info.Name, seqset.String())

	defer close(ch)

	// No seqset, ignore
	if seqset == nil {
		klog.Errorf("obj=mailbox verb=LIST user-id='%s' msg='list mailbox %s messages no seqset found'",
			m.owner, m.info.Name)
		return nil
	}

	messages, err := GDatabase.ListMessages(m.id, seqset, uid)

	if err != nil {
		klog.Errorf("Unable to list messages in mailbox %s for user %s: %s", m.info.Name, m.owner, err)
		return err
	}

	for _, m := range messages {
		msg, err := m.Fetch(m.SeqNum, items)
		if err != nil {
			return fmt.Errorf("unable to perform ListMessages. Failed to fetch seqnum %d: %s", m.SeqNum, err)
		}

		ch <- msg

		if m.HasFlag(imap.RecentFlag) {
			m.Flags = backendutil.UpdateFlags(m.Flags, imap.RemoveFlags, []string{imap.RecentFlag})
			if err := GDatabase.UpdateMessageFlags(m); err != nil {
				return err
			}
		}
	}
	return nil
}

func (m Mailbox) SearchMessages(uid bool, criteria *imap.SearchCriteria) ([]uint32, error) {
	mailboxMetrics.counterCmd.WithLabelValues("SEARCH", m.owner).Inc()
	startTime := time.Now().Nanosecond()
	defer mailboxMetrics.counterTimeCmd.WithLabelValues("SEARCH", m.owner).Add(float64(time.Now().Nanosecond()) - float64(startTime))

	klog.V(2).Infof("obj=mailbox verb=SEARCH user-id='%s' msg='search mailbox %s messages'", m.owner, m.info.Name)

	messages, err := GDatabase.ListMessages(m.id, nil, false)
	if err != nil {
		klog.Errorf("Unable to list messages in mailbox %s for user %s: %s", m.info.Name, m.owner, err)
		return nil, err
	}

	if GConfig.Debug {
		klog.Infof("search messages by uid=(%t), criteria: %v", uid, criteria)
	}

	var ids []uint32

	if messages == nil {
		return nil, nil
	}
	for _, msg := range messages {
		if GConfig.Debug {
			klog.Infof("SearchMessages: check validity for uid=%d, seqNum=%d", msg.Uid, msg.SeqNum)
		}

		ok, err := msg.Match(msg.SeqNum, criteria)
		if err != nil || !ok {
			continue
		}

		var id uint32
		if uid {
			id = msg.Uid
		} else {
			id = msg.SeqNum
		}

		if GConfig.Debug {
			klog.Infof("SearchMessages: %d (uid=%d, seqNum=%d) is valid", id, msg.Uid, msg.SeqNum)
		}

		ids = append(ids, id)
	}
	return ids, nil
}

func (m Mailbox) CreateMessage(flags []string, date time.Time, body imap.Literal) error {
	mailboxMetrics.counterCmd.WithLabelValues("CREATE_MSG", m.owner).Inc()
	startTime := time.Now().Nanosecond()
	defer mailboxMetrics.counterTimeCmd.WithLabelValues("CREATE_MSG", m.owner).Add(float64(time.Now().Nanosecond()) - float64(startTime))

	klog.V(2).Infof("obj=mailbox verb=CREATE_MSG user-id='%s' msg='create message in mailbox %s", m.owner, m.info.Name)

	recentFound := false
	for _, f := range flags {
		if f == imap.RecentFlag {
			recentFound = true
			break
		}
	}

	if !recentFound {
		flags = append(flags, imap.RecentFlag)
	}

	bToReadLen := body.Len()
	bodyRaw := make([]byte, bToReadLen)

	bLen, err := body.Read(bodyRaw)
	if err != nil {
		return fmt.Errorf("unable to create message, reading error: %s", err.Error())
	}

	if bLen != bToReadLen {
		return fmt.Errorf("unable to create message, read %d over %d bytes", bLen, body.Len())
	}

	err = GDatabase.CreateMessage(m.id, flags, date, bodyRaw)
	if err != nil {
		return err
	}

	mbStatus, err := m.Status([]imap.StatusItem{imap.StatusMessages})
	if err != nil {
		return err
	}

	mbStatus.Flags = nil
	mbStatus.PermanentFlags = nil

	m.updates <- backend.MailboxUpdate{
		Update:        backend.NewUpdate(m.owner, m.info.Name),
		MailboxStatus: mbStatus,
	}
	return nil
}

func (m Mailbox) UpdateMessagesFlags(uid bool, seqset *imap.SeqSet, operation imap.FlagsOp, flags []string) error {
	mailboxMetrics.counterCmd.WithLabelValues("UPDATE_MSG", m.owner).Inc()
	startTime := time.Now().Nanosecond()
	defer mailboxMetrics.counterTimeCmd.WithLabelValues("UPDATE_MSG", m.owner).Add(float64(time.Now().Nanosecond()) - float64(startTime))

	klog.V(2).Infof("obj=mailbox verb=UPDATE_MSG user-id='%s' seqset='%s' msg='update mailbox %s messages flags (%v)'",
		m.owner, seqset.String(), m.info.Name, flags)

	messages, err := GDatabase.ListMessages(m.id, seqset, uid)

	if err != nil {
		klog.Errorf("Unable to list messages in mailbox %s for user %s: %s", m.info.Name, m.owner, err)
		return err
	}

	for _, msg := range messages {
		msg.Flags = backendutil.UpdateFlags(msg.Flags, operation, flags)
		if err := GDatabase.UpdateMessageFlags(msg); err != nil {
			return err
		}

		update := backend.MessageUpdate{
			Update:  backend.NewUpdate(m.owner, m.info.Name),
			Message: &msg.Message,
		}
		m.updates <- update
	}

	return nil
}

func (m Mailbox) CopyMessages(uid bool, seqset *imap.SeqSet, dest string) error {
	mailboxMetrics.counterCmd.WithLabelValues("COPY_MSG", m.owner).Inc()
	startTime := time.Now().Nanosecond()
	defer mailboxMetrics.counterTimeCmd.WithLabelValues("COPY_MSG", m.owner).Add(float64(time.Now().Nanosecond()) - float64(startTime))

	klog.V(2).Infof("obj=mailbox verb=COPY_MSG user-id='%s' msg='copy mailbox %s messages to %s (seqset %s)",
		m.owner, m.info.Name, dest, seqset.String())

	mbDest := NewMailbox(m.owner, m.updates)
	err := GDatabase.getMailbox(m.owner, dest, mbDest)
	if err != nil {
		klog.Errorf("Unable to get mailbox mailbox %s for user-id %s: %s", m.info.Name, m.owner, err)
		return err
	}

	messages, err := GDatabase.ListMessages(m.id, seqset, uid)

	if err != nil {
		klog.Errorf("Unable to list messages in mailbox %s for user %s: %s", m.info.Name, m.owner, err)
		return err
	}

	for _, msg := range messages {
		newMsg := *msg
		newMsg.AddFlag(imap.RecentFlag)

		if err := GDatabase.CreateMessage(mbDest.id, newMsg.Flags, newMsg.InternalDate, newMsg.Body); err != nil {
			return err
		}
	}

	mbStatus, err := m.Status([]imap.StatusItem{imap.StatusMessages})
	if err != nil {
		return err
	}

	mbStatus.Flags = nil
	mbStatus.PermanentFlags = nil

	m.updates <- backend.MailboxUpdate{
		Update:        backend.NewUpdate(m.owner, m.info.Name),
		MailboxStatus: mbStatus,
	}

	return nil
}

func (m Mailbox) MoveMessages(uid bool, seqset *imap.SeqSet, dest string) error {
	mailboxMetrics.counterCmd.WithLabelValues("MOVE_MSG", m.owner).Inc()
	startTime := time.Now().Nanosecond()
	defer mailboxMetrics.counterTimeCmd.WithLabelValues("MOVE_MSG", m.owner).Add(float64(time.Now().Nanosecond()) - float64(startTime))

	klog.V(2).Infof("obj=mailbox verb=MOVE_MSG user-id='%s' msg='move mailbox %s messages to %s (seqset %s)",
		m.owner, m.info.Name, dest, seqset.String())

	mbDest := NewMailbox(m.owner, m.updates)
	err := GDatabase.getMailbox(m.owner, dest, mbDest)
	if err != nil {
		return err
	}

	messages, err := GDatabase.ListMessages(m.id, seqset, uid)

	for _, msg := range messages {
		newMsg := *msg
		newMsg.AddFlag(imap.RecentFlag)

		if err := GDatabase.MoveMessage(msg.Uid, mbDest.id, newMsg.Flags, newMsg.InternalDate, newMsg.Body); err != nil {
			return err
		}

		m.updates <- &backend.ExpungeUpdate{
			Update: backend.NewUpdate(m.owner, m.info.Name),
			SeqNum: msg.SeqNum,
		}
	}

	return nil
}

func (m Mailbox) Expunge() error {
	mailboxMetrics.counterCmd.WithLabelValues("EXPUNGE", m.owner).Inc()
	startTime := time.Now().Nanosecond()
	defer mailboxMetrics.counterTimeCmd.WithLabelValues("EXPUNGE", m.owner).Add(float64(time.Now().Nanosecond()) - float64(startTime))

	klog.V(2).Infof("obj=mailbox verb=EXPUNGE user-id='%s' msg='expunge mailbox %s'", m.owner, m.info.Name)

	criteria := &imap.SearchCriteria{
		WithFlags: []string{imap.DeletedFlag},
	}

	seqnums, err := m.SearchMessages(false, criteria)
	if err != nil {
		return err
	}

	if err := GDatabase.Expunge(m.id, m.info.Name == MailboxTrash); err != nil {
		return err
	}

	// Iterate sequence numbers from the last one to the first one, as deleting
	// messages changes their respective numbers
	for i := len(seqnums) - 1; i >= 0; i-- {
		m.updates <- &backend.ExpungeUpdate{
			Update: backend.NewUpdate(m.owner, m.info.Name),
			SeqNum: seqnums[i],
		}
	}
	return nil
}
