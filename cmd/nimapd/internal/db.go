package internal

import (
	"database/sql"
	"encoding/base64"
	"fmt"
	"time"

	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/backend"
	"gitlab.com/nerzhul/nsmtpd/pkg/nimapd/message"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file" // golang-migrate requires blank import
	"github.com/lib/pq"
	_ "github.com/lib/pq" // pq requires blank import
	"k8s.io/klog/v2"
)

type dbConfig struct {
	URL             string `yaml:"url"`
	MaxIdleConns    int    `yaml:"max-idle-conns"`
	MaxOpenConns    int    `yaml:"max-open-conns"`
	MigrationSource string
}

const (
	validationQuery            = "SELECT 1"
	getUserNameFromID          = "SELECT id FROM users WHERE username = $1"
	getUsernameFromMailQuery   = "SELECT username FROM users WHERE id IN (SELECT user_id FROM user_mail_addresses WHERE user_address = $1)"
	getUserSecretQuery         = `SELECT password, salt FROM users WHERE id = $1`
	registerUserQuery          = `INSERT INTO users(id, username, password, salt) VALUES (uuid_generate_v4(), $1, $2, $3) RETURNING id`
	getUserMailboxes           = "SELECT id, name, subscribed FROM user_mailboxes WHERE user_id = $1"
	getUserSubscribedMailboxes = "SELECT id, name, subscribed FROM user_mailboxes WHERE user_id = $1 AND subscribed = 't'"

	isMailboxyExistsQuery     = `SELECT EXISTS(SELECT 1 FROM user_mailboxes WHERE user_id = $1 AND name = $2)`
	getUserMailboxQuery       = `SELECT id, name, subscribed FROM user_mailboxes WHERE user_id = $1 AND name = $2`
	getMailboxAttributes      = `SELECT attr FROM user_mailbox_attributes WHERE mailbox_id = $1`
	createMailboxQuery        = `INSERT INTO user_mailboxes(name, user_id, subscribed) VALUES ($1, $2, 't')`
	deleteMailboxQuery        = `DELETE FROM user_mailboxes CASCADE WHERE name = $1 AND user_id = $2`
	renameMailboxQuery        = `UPDATE user_mailboxes SET name = $3 WHERE name = $1 AND user_id = $2`
	renameChildMailboxesQuery = `UPDATE user_mailboxes SET name = regexp_replace(name, concat('^', $1), $3) WHERE name LIKE concat($1::text, '%') AND user_id = $2`
	setMailboxSubscribedQuery = `UPDATE user_mailboxes SET subscribed = $2 WHERE id = $1`

	createMailQuery                         = `INSERT INTO mails(mailbox_id, date, body) VALUES($1, $2, $3 ) RETURNING id`
	deleteMailQuery                         = `DELETE FROM mails WHERE id = $1`
	setMailFlagQuery                        = `INSERT INTO mail_flags(mail_id, flag) VALUES($1, $2) ON CONFLICT DO NOTHING`
	listMailsQuery                          = `SELECT id, date, body, ROW_NUMBER () OVER (ORDER BY id) AS seqnum FROM mails`
	listMailsInMailboxQuery                 = `SELECT id, date, body, ROW_NUMBER () OVER (ORDER BY id) AS seqnum FROM mails WHERE mailbox_id = $1`
	listMailsInMailboxIdFilteredQuery       = `SELECT id, date, body, ROW_NUMBER () OVER (ORDER BY id) AS seqnum FROM mails WHERE mailbox_id = $1 AND id = ANY($2)`
	listMailboxMailIDSeqSetQuery            = `SELECT id, ROW_NUMBER () OVER (ORDER BY id) AS seqnum FROM mails WHERE mailbox_id = $1`
	getFlagsFromMailsQuery         = `SELECT mail_id, flag FROM mail_flags WHERE mail_id IN (SELECT id FROM mails)`
	getFlagsFromMailsInMailboxQuery         = `SELECT mail_id, flag FROM mail_flags WHERE mail_id IN (SELECT id FROM mails WHERE mailbox_id = $1)`
	getFlagsFromMailsInMailboxFilteredQuery = `SELECT mail_id, flag FROM mail_flags WHERE mail_id IN (SELECT id FROM mails WHERE mailbox_id = $1 AND id = ANY($2))`
	getMailboxMessageCountQuery             = `SELECT COUNT(id) FROM mails WHERE mailbox_id = $1`
	getMailboxMessageCountByFlagQuery       = `SELECT COUNT(*) FROM mails INNER JOIN mail_flags ON mails.id = mail_flags.mail_id ` +
		`WHERE mails.mailbox_id = $1 AND mail_flags.flag = $2`
	getMailboxMessageFlagsQuery       = `SELECT flag FROM mails INNER JOIN mail_flags ON mails.id = mail_flags.mail_id WHERE mails.mailbox_id = $1 GROUP BY (flag);`
	getMailboxUnseenMessageCountQuery = `SELECT COUNT(*) FROM (SELECT id,flag FROM mails ` +
		`LEFT OUTER JOIN mail_flags ON mails.id = mail_flags.mail_id AND mail_flags.flag = '` + imap.SeenFlag + `' WHERE mails.mailbox_id = $1) as foo WHERE flag IS NULL;`
	getMailboxFirstUnseenSeqNumQuery = `SELECT seqnum FROM (SELECT id, date, ROW_NUMBER () OVER (ORDER BY id) as seqnum,flag FROM mails ` +
		`LEFT OUTER JOIN mail_flags on mail_flags.mail_id = mails.id AND mail_flags.flag = '` + imap.SeenFlag + `' WHERE mails.mailbox_id = $1) as foo WHERE flag IS NULL ORDER BY seqnum LIMIT 1;
`
	clearMailboxQuery    = `DELETE FROM mails WHERE mailbox_id = $1`
	expungeMailboxQuery  = clearMailboxQuery + ` AND id IN (SELECT mail_id FROM mail_flags WHERE flag = '` + imap.DeletedFlag + `')`
	deleteMailFlagsQuery = `DELETE FROM mail_flags WHERE mail_id = $1`

	// routing
	getMailboxRouteQuery = `SELECT mailbox_route FROM user_mailbox_route WHERE user_id = $1 AND route_type = $2 AND condition = $3`

	getNextMailIDQuery = `SELECT last_value FROM mails_id_seq;`
)

type Database struct {
	nativeDB *sql.DB
	Config   *dbConfig
}

var GDatabase Database

func (db *Database) Init() bool {
	if db.Config == nil {
		klog.Errorf("DB config is nil !!!")
		return false
	}

	klog.Infof("Connecting to database at %s", db.Config.URL)
	nativeDB, err := sql.Open("postgres", db.Config.URL)
	if err != nil {
		klog.Errorf("Failed to connect to DB: %s", err)
		return false
	}

	db.nativeDB = nativeDB
	if !db.ValidationQuery() {
		db.nativeDB = nil
		return false
	}

	db.nativeDB.SetMaxIdleConns(db.Config.MaxIdleConns)
	db.nativeDB.SetMaxOpenConns(db.Config.MaxOpenConns)

	if !db.runMigrations() {
		db.nativeDB = nil
		return false
	}

	klog.Infof("Connected to DB.")
	return true
}

func (db *Database) runMigrations() bool {
	klog.Infof("Checking for schema migrations to perform...")
	nativeDB, err := sql.Open("postgres", db.Config.URL)
	if err != nil {
		klog.Errorf("Failed to connect to ReleaseChecker DB while running migrations: %s", err)
		return false
	}

	driver, err := postgres.WithInstance(nativeDB, &postgres.Config{})
	if err != nil {
		klog.Errorf("Unable to create migration instance on ReleaseChecker DB: %s", err)
		return false
	}

	m, err := migrate.NewWithDatabaseInstance(
		db.Config.MigrationSource,
		"postgres", driver)

	if err != nil {
		klog.Errorf("Unable to run migrations on ReleaseChecker DB: %s", err)
		return false
	}

	defer m.Close()

	err = m.Up()
	if err != nil && err != migrate.ErrNoChange {
		klog.Errorf("Database Migration failed: %s", err)
		return false
	}

	klog.Infof("Schema migrations done.")
	return true
}

func (db *Database) ValidationQuery() bool {
	rows, err := db.nativeDB.Query(validationQuery)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		klog.Errorf("Failed to run validation query: %s", err)
		return false
	}

	return true
}

func (db *Database) GetUserID(username string) (string, error) {
	rows, err := db.nativeDB.Query(getUserNameFromID, username)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return "", fmt.Errorf("failed to run getUserID query: %s", err)
	}

	if rows.Next() {
		var id string
		if err := rows.Scan(&id); err != nil {
			return "", fmt.Errorf("unable to getUserID: %s", err)
		}

		return id, nil
	}

	return "", nil
}

func (db *Database) RegisterUser(username string, password string, salt string) (string, error) {
	var userID string
	if err := db.nativeDB.QueryRow(registerUserQuery, username, password, salt).Scan(&userID); err != nil {
		return "", fmt.Errorf("failed to registerUserQuery for username %s: %s", username, err)
	}

	return userID, nil
}

func (db *Database) GetUserSecrets(userID string) ([]byte, string, error) {
	rows, err := db.nativeDB.Query(getUserSecretQuery, userID)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return nil, "", fmt.Errorf("failed to run GetUserSecrets query: %s", err)
	}

	if rows.Next() {
		var hashedPassword []byte
		var salt string
		if err := rows.Scan(&hashedPassword, &salt); err != nil {
			return nil, "", fmt.Errorf("unable to GetUserSecrets: %s", err)
		}

		return hashedPassword, salt, nil
	}

	return nil, "", fmt.Errorf("user %s not found in db", userID)
}

func (db *Database) ListMailboxes(user *User, subscribed bool) ([]backend.Mailbox, error) {
	q := getUserMailboxes
	if subscribed {
		q = getUserSubscribedMailboxes
	}

	rows, err := db.nativeDB.Query(q, user.getID())
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return nil, fmt.Errorf("failed to run listMailboxes query: %s", err)
	}

	var mbList []backend.Mailbox

	for rows.Next() {
		mb := NewMailbox(user.getID(), user.updates)
		var subscribed bool
		if err := rows.Scan(&mb.id, &mb.info.Name, &subscribed); err != nil {
			return nil, fmt.Errorf("unable to scan mailbox on ListMailboxes: %s", err)
		}

		mb.info.Delimiter = IMAPDelimiter
		mb.info.Attributes, err = db.getMailboxAttributes(mb.id)
		if err != nil {
			return nil, fmt.Errorf("unable to scan mailbox attributes on ListMailboxes: %s", err)
		}
		mbList = append(mbList, mb)
	}

	return mbList, nil
}

func (db *Database) getMailboxAttributes(id int64) ([]string, error) {
	rows, err := db.nativeDB.Query(getMailboxAttributes, id)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return nil, fmt.Errorf("failed to run getMailboxAttributes for id %d: %s", id, err)
	}

	var attrs []string
	for rows.Next() {
		var attr string
		if err := rows.Scan(&attr); err != nil {
			return nil, fmt.Errorf("unable to scan mailbox attributes for id %d: %s", id, err)
		}

		attrs = append(attrs, attr)
	}

	return attrs, nil
}

func (db *Database) createMailbox(user *User, name string) error {
	if _, err := db.nativeDB.Exec(createMailboxQuery, name, user.getID()); err != nil {
		return fmt.Errorf("failed to run createMailbox query for user %s and mailbox %s: %s",
			user.Username(), name, err)
	}

	return nil
}

func (db *Database) deleteMailbox(user *User, name string) error {
	if _, err := db.nativeDB.Exec(deleteMailboxQuery, name, user.getID()); err != nil {
		return fmt.Errorf("failed to run deleteMailbox query for user %s and mailbox %s: %s",
			user.Username(), name, err)
	}

	return nil
}

func (db *Database) renameMailbox(user *User, name string, newName string) error {
	tx, err := db.nativeDB.Begin()
	if err != nil {
		return fmt.Errorf("unable to begin transaction on create message: %s", err.Error())
	}

	if _, err := tx.Exec(renameMailboxQuery, name, user.getID(), newName); err != nil {
		return fmt.Errorf("failed to run renameMailbox query for user %s and mailbox %s: %s",
			user.Username(), name, err)
	}

	if _, err := tx.Exec(renameChildMailboxesQuery, name, user.getID(), newName); err != nil {
		return fmt.Errorf("failed to run renameMailbox query (child) for user %s and mailbox %s: %s",
			user.Username(), name, err)
	}

	if err := tx.Commit(); err != nil {
		return fmt.Errorf("unable to commit on renameMailbox: %s", err.Error())
	}

	return nil
}

func (db *Database) getMailbox(userID string, name string, mb *Mailbox) error {
	rows, err := db.nativeDB.Query(getUserMailboxQuery, userID, name)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return fmt.Errorf("failed to run getMailbox query: %s", err)
	}

	if rows.Next() {
		var subscribed bool
		if err := rows.Scan(&mb.id, &mb.info.Name, &subscribed); err != nil {
			return fmt.Errorf("unable to scan mailbox on getMailbox query: %s", err)
		}

		mb.info.Delimiter = IMAPDelimiter
		mb.info.Attributes, err = db.getMailboxAttributes(mb.id)
		if err != nil {
			return fmt.Errorf("unable to scan mailbox attributes on getMailbox query: %s", err)
		}

		return nil
	}

	return fmt.Errorf("no mailbox %s for user %s", name, userID)
}

func (db *Database) setSubscribed(id int64, subscribed bool) error {
	if _, err := db.nativeDB.Exec(setMailboxSubscribedQuery, id, subscribed); err != nil {
		return fmt.Errorf("failed to run setSubscribed query for id %d: %s", id, err)
	}

	return nil
}

func (db *Database) listMailboxMessageIDs(mbID int64, seqSet *imap.SeqSet, byUID bool) ([]uint32, error) {
	if seqSet == nil {
		klog.Fatal("nil seqSet in ListMailboxMessageIDs")
		return nil, fmt.Errorf("nil seqSet in ListMailboxMessageIDs")
	}

	rows, err := db.nativeDB.Query(listMailboxMailIDSeqSetQuery, mbID)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return nil, fmt.Errorf("failed to run listMailsInMailboxQuery query: %s", err)
	}

	idList := make([]uint32, 0)
	for rows.Next() {
		var msgUid, msgSeqNum uint32
		if err := rows.Scan(&msgUid, &msgSeqNum); err != nil {
			return nil, fmt.Errorf("unable to scan mail on listMailsInMailboxQuery: %s", err)
		}

		var id uint32
		if byUID {
			id = msgUid
		} else {
			id = msgSeqNum
		}

		// Ignore non valid id
		if !seqSet.Contains(id) {
			continue
		}

		idList = append(idList, msgUid)
	}

	return idList, nil
}

func (db *Database) ListAllMessages() ([]*message.Message, error) {
	rows, err := db.nativeDB.Query(listMailsQuery)

	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return nil, fmt.Errorf("failed to run listMailsQuery query: %s", err)
	}

	messages := make([]*message.Message, 0)
	for rows.Next() {
		message := message.New()

		var encBody []byte
		if err := rows.Scan(&message.Uid, &message.InternalDate, &encBody, &message.SeqNum); err != nil {
			return nil, fmt.Errorf("unable to scan mail on listMailsQuery: %s", err)
		}

		decLen := base64.StdEncoding.DecodedLen(len(encBody))
		message.Body = make([]byte, decLen)

		_, err := base64.StdEncoding.Decode(message.Body, encBody)
		if err != nil {
			return nil, fmt.Errorf("unable to decode message body on listMailsQuery: %s", err)
		}

		message.Size = uint32(len(message.Body))

		messages = append(messages, message)
	}

	// Now fetch flags
	var flagRows *sql.Rows
	flagRows, err = db.nativeDB.Query(getFlagsFromMailsQuery)

	if flagRows != nil {
		defer flagRows.Close()
	}

	if err != nil {
		return nil, fmt.Errorf("failed to run getFlagsFromMailsQuery query: %s", err)
	}

	messageFlags := make(map[int64][]string, 0)
	for flagRows.Next() {
		var id int64
		var flag string
		if err := flagRows.Scan(&id, &flag); err != nil {
			return nil, fmt.Errorf("unable to scan flags on getFlagsFromMailsQuery: %s", err)
		}

		if messageFlags[id] == nil {
			messageFlags[id] = make([]string, 0)
		}

		messageFlags[id] = append(messageFlags[id], flag)
	}

	// Now apply flags
	for _, message := range messages {
		message.Flags = messageFlags[int64(message.Uid)]
	}

	return messages, nil
}

func (db *Database) ListMessages(mbID int64, seqSet *imap.SeqSet, byUID bool) ([]*message.Message, error) {
	var rows *sql.Rows
	var err error

	uidList := make([]uint32, 0)
	if seqSet == nil {
		rows, err = db.nativeDB.Query(listMailsInMailboxQuery, mbID)
	} else { // If seqSet is present, we need to filter messages before fetching them all
		uidList, err = db.listMailboxMessageIDs(mbID, seqSet, byUID)
		if err != nil {
			return nil, err
		}

		rows, err = db.nativeDB.Query(listMailsInMailboxIdFilteredQuery, mbID, pq.Array(uidList))
	}

	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return nil, fmt.Errorf("failed to run listMailsInMailboxQuery query: %s", err)
	}

	messages := make([]*message.Message, 0)
	for rows.Next() {
		message := message.New()

		var encBody []byte
		if err := rows.Scan(&message.Uid, &message.InternalDate, &encBody, &message.SeqNum); err != nil {
			return nil, fmt.Errorf("unable to scan mail on listMailsInMailboxQuery: %s", err)
		}

		decLen := base64.StdEncoding.DecodedLen(len(encBody))
		message.Body = make([]byte, decLen)

		_, err := base64.StdEncoding.Decode(message.Body, encBody)
		if err != nil {
			return nil, fmt.Errorf("unable to decode message body on listMailsInMailboxQuery: %s", err)
		}

		message.Size = uint32(len(message.Body))

		messages = append(messages, message)
	}

	// Now fetch flags
	var flagRows *sql.Rows
	if seqSet == nil {
		flagRows, err = db.nativeDB.Query(getFlagsFromMailsInMailboxQuery, mbID)
	} else {
		flagRows, err = db.nativeDB.Query(getFlagsFromMailsInMailboxFilteredQuery, mbID, pq.Array(uidList))
	}

	if flagRows != nil {
		defer flagRows.Close()
	}

	if err != nil {
		return nil, fmt.Errorf("failed to run getFlagsFromMailsInMailboxQuery query: %s", err)
	}

	messageFlags := make(map[int64][]string, 0)
	for flagRows.Next() {
		var id int64
		var flag string
		if err := flagRows.Scan(&id, &flag); err != nil {
			return nil, fmt.Errorf("unable to scan flags on getFlagsFromMailsInMailboxQuery: %s", err)
		}

		if messageFlags[id] == nil {
			messageFlags[id] = make([]string, 0)
		}

		messageFlags[id] = append(messageFlags[id], flag)
	}

	// Now apply flags
	for _, message := range messages {
		message.Flags = messageFlags[int64(message.Uid)]
	}

	return messages, nil
}

func (db *Database) GetMailboxFirstUnseenSeqNum(mbID int64) (uint32, error) {
	rows, err := db.nativeDB.Query(getMailboxFirstUnseenSeqNumQuery, mbID)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return 0, fmt.Errorf("failed to run getMailboxFirstUnseenSeqNumQuery query: %s", err)
	}

	if rows.Next() {
		var count uint32
		if err := rows.Scan(&count); err != nil {
			return 0, err
		}

		return count, nil
	}

	return 1, nil
}

func (db *Database) CreateMessage(mbID int64, flags []string, date time.Time, body []byte) error {
	tx, err := db.nativeDB.Begin()
	if err != nil {
		return fmt.Errorf("unable to begin transaction on create message: %s", err.Error())
	}

	bodyB64 := make([]byte, base64.StdEncoding.EncodedLen(len(body)))
	base64.StdEncoding.Encode(bodyB64, body)

	var mailID int64
	if err := tx.QueryRow(createMailQuery, mbID, date, bodyB64).Scan(&mailID); err != nil {
		return fmt.Errorf("failed to createMailQuery for mbID %d: %s", mbID, err)
	}

	for _, f := range flags {
		_, err := tx.Exec(setMailFlagQuery, mailID, f)
		if err != nil {
			return fmt.Errorf("failed to setMailFlagQuery for mail ID %d: %s", mailID, err)
		}
	}

	if err := tx.Commit(); err != nil {
		return fmt.Errorf("unable to commit on create message: %s", err.Error())
	}

	return nil
}

func (db *Database) MoveMessage(msgUID uint32, mbID int64, flags []string, date time.Time, body []byte) error {
	tx, err := db.nativeDB.Begin()
	if err != nil {
		return fmt.Errorf("unable to begin transaction on create message: %s", err.Error())
	}

	bodyB64 := make([]byte, base64.StdEncoding.EncodedLen(len(body)))
	base64.StdEncoding.Encode(bodyB64, body)

	var mailID int64
	if err := tx.QueryRow(createMailQuery, mbID, date, bodyB64).Scan(&mailID); err != nil {
		return fmt.Errorf("failed to createMailQuery for mbID %d: %s", mbID, err)
	}

	for _, f := range flags {
		_, err := tx.Exec(setMailFlagQuery, mailID, f)
		if err != nil {
			return fmt.Errorf("failed to setMailFlagQuery for mail ID %d: %s", mailID, err)
		}
	}

	if _, err := tx.Exec(deleteMailQuery, msgUID); err != nil {
		return fmt.Errorf("failed to createMailQuery for mbID %d: %s", mbID, err)
	}

	if err := tx.Commit(); err != nil {
		return fmt.Errorf("unable to commit on create message: %s", err.Error())
	}

	return nil
}

func (db *Database) UpdateMessageFlags(msg *message.Message) error {
	tx, err := db.nativeDB.Begin()
	if err != nil {
		return fmt.Errorf("unable to begin transaction on update message flags: %s", err.Error())
	}

	if _, err := tx.Exec(deleteMailFlagsQuery, msg.Uid); err != nil {
		return fmt.Errorf("unable to drop mails flags on message flag update: %s", err.Error())
	}

	for _, f := range msg.Flags {
		if _, err := tx.Exec(setMailFlagQuery, msg.Uid, f); err != nil {
			return fmt.Errorf("unable to drop mails flags on message flag update: %s", err.Error())
		}
	}

	if err := tx.Commit(); err != nil {
		return fmt.Errorf("unable to commit on update message flags: %s", err.Error())
	}

	return nil
}

func (db *Database) GetMailboxMessageCount(mbID int64) (uint32, error) {
	rows, err := db.nativeDB.Query(getMailboxMessageCountQuery, mbID)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return 0, fmt.Errorf("failed to run getMailboxMessageCountQuery query: %s", err)
	}

	if rows.Next() {
		var count uint32
		if err := rows.Scan(&count); err != nil {
			return 0, err
		}

		return count, nil
	}

	return 0, fmt.Errorf("no rows returned on getMailboxMessageCountQuery")
}

func (db *Database) GetMailboxMessageCountByFlag(mbID int64, flag string) (uint32, error) {
	rows, err := db.nativeDB.Query(getMailboxMessageCountByFlagQuery, mbID, flag)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return 0, fmt.Errorf("failed to run getMailboxMessageCountByFlagQuery query: %s", err)
	}

	if rows.Next() {
		var count uint32
		if err := rows.Scan(&count); err != nil {
			return 0, err
		}

		return count, nil
	}

	return 0, fmt.Errorf("no rows returned on getMailboxMessageCountByFlagQuery")
}

func (db *Database) ListMailFlagsForMailbox(mbID int64) ([]string, error) {
	rows, err := db.nativeDB.Query(getMailboxMessageFlagsQuery, mbID)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return nil, fmt.Errorf("failed to run getMailboxMessageFlagsQuery query: %s", err)
	}

	flags := make([]string, 0)
	for rows.Next() {
		var flag string
		if err := rows.Scan(&flag); err != nil {
			return nil, err
		}

		flags = append(flags, flag)
		return flags, nil
	}

	return nil, nil
}

func (db *Database) GetMailboxUnseenMessageCount(mbID int64) (uint32, error) {
	rows, err := db.nativeDB.Query(getMailboxUnseenMessageCountQuery, mbID)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return 0, fmt.Errorf("failed to run getMailboxUnseenMessageCountQuery query: %s", err)
	}

	if rows.Next() {
		var count uint32
		if err := rows.Scan(&count); err != nil {
			return 0, err
		}

		return count, nil
	}

	return 0, fmt.Errorf("no rows returned on getMailboxUnseenMessageCountQuery")
}

func (db *Database) Expunge(mbID int64, forceClear bool) error {
	tx, err := db.nativeDB.Begin()
	if err != nil {
		return fmt.Errorf("unable to begin transaction on expunge: %s", err.Error())
	}

	query := expungeMailboxQuery
	if forceClear {
		query = clearMailboxQuery
	}

	if _, err := tx.Exec(query, mbID); err != nil {
		return fmt.Errorf("unable to run expunge query: %s", err.Error())
	}

	if err := tx.Commit(); err != nil {
		return fmt.Errorf("unable to commit on expunge: %s", err.Error())
	}

	return nil
}

func (db *Database) GetNextMailUID() (uint32, error) {
	rows, err := db.nativeDB.Query(getNextMailIDQuery)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return 0, fmt.Errorf("failed to run getNextMailIDQuery query: %s", err)
	}

	if rows.Next() {
		var id uint32
		if err := rows.Scan(&id); err != nil {
			return 0, err
		}

		return id, nil
	}

	return 1, nil
}

func (db *Database) isMailboxExists(userID string, name string) (bool, error) {
	rows, err := db.nativeDB.Query(isMailboxyExistsQuery, userID, name)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return false, fmt.Errorf("failed to run isMailboxyExistsQuery query: %s", err)
	}

	if rows.Next() {
		var exists bool
		if err := rows.Scan(&exists); err != nil {
			return false, err
		}

		return exists, nil
	}

	return false, nil
}

func (db *Database) getMailboxAddressRoute(userID string, addr string, condition string) (string, error) {
	rows, err := db.nativeDB.Query(getMailboxRouteQuery, userID, condition, addr)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return "", fmt.Errorf("failed to run getMailboxRoute query: %s", err)
	}

	if rows.Next() {
		var route string
		if err := rows.Scan(&route); err != nil {
			return "", fmt.Errorf("unable to scan getMailboxRoute: %s", err)
		}

		return route, nil
	}

	return "", nil
}

func (db *Database) getUsernameFromMail(addr string) (string, error) {
	rows, err := db.nativeDB.Query(getUsernameFromMailQuery, addr)
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return "", fmt.Errorf("failed to run getUsernameFromMail query: %s", err)
	}

	if rows.Next() {
		var username string
		if err := rows.Scan(&username); err != nil {
			return "", fmt.Errorf("unable to scan getUsernameFromMail: %s", err)
		}

		return username, nil
	}

	return "", nil
}
