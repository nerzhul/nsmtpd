package internal

import (
	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/commands"
	"github.com/emersion/go-imap/responses"
	"github.com/emersion/go-imap/server"
)

type ListCommandOverwrite struct {
	commands.List
}

func (cmd *ListCommandOverwrite) Handle(conn server.Conn) error {
	ctx := conn.Context()
	if ctx.User == nil {
		return server.ErrNotAuthenticated
	}

	ch := make(chan *imap.MailboxInfo)
	res := &responses.List{Mailboxes: ch, Subscribed: cmd.Subscribed}

	done := make(chan error, 1)
	go (func() {
		done <- conn.WriteResp(res)
		// Make sure to drain the channel.
		for _ = range ch {
		}
	})()

	if cmd.Mailbox == "" {
		ch <- &imap.MailboxInfo{
			Attributes: []string{imap.NoSelectAttr},
			Delimiter:  IMAPDelimiter,
			Name:       IMAPDelimiter,
		}
		close(ch)
		return <-done
	}

	mailboxes, err := ctx.User.ListMailboxes(cmd.Subscribed)
	if err != nil {
		// Close channel to signal end of results
		close(ch)
		return err
	}

	for _, mbox := range mailboxes {
		info, err := mbox.Info()
		if err != nil {
			// Close channel to signal end of results
			close(ch)
			return err
		}

		if info.Match(cmd.Reference, cmd.Mailbox) {
			ch <- info
		}
	}
	// Close channel to signal end of results
	close(ch)

	return <-done
}
