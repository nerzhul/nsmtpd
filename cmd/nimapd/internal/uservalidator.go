package internal

type UserValidator interface {
	Login(username string, password string) (string, error)
	Exists(username string) (bool, error)
}
