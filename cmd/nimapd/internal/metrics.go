package internal

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var mailboxMetrics *mailboxCounters
var messageMetrics *messageCounters

type mailboxCounters struct {
	counterCmd     *prometheus.CounterVec
	counterTimeCmd *prometheus.CounterVec
}

type messageCounters struct {
	counterCmd     *prometheus.CounterVec
	counterTimeCmd *prometheus.CounterVec
}

func initMetrics() {
	mailboxMetrics = &mailboxCounters{
		counterCmd: promauto.NewCounterVec(
			prometheus.CounterOpts{
				Name: "nimapd_mailbox_requests_total",
				Help: "mailboxes request count",
			},
			[]string{"command", "user_id"}),
		counterTimeCmd: promauto.NewCounterVec(
			prometheus.CounterOpts{
				Name: "nimapd_mailbox_request_total_time",
				Help: "Total time spent in requests",
			},
			[]string{"command", "user_id"}),
	}

	messageMetrics = &messageCounters{
		counterCmd: promauto.NewCounterVec(
			prometheus.CounterOpts{
				Name: "nimapd_message_requests_total",
				Help: "message request count",
			},
			[]string{"command"}),
		counterTimeCmd: promauto.NewCounterVec(
			prometheus.CounterOpts{
				Name: "nimapd_message_request_total_time",
				Help: "Total time spent in requests",
			},
			[]string{"command"}),
	}
}
