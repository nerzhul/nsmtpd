package internal

import (
	"flag"
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"k8s.io/klog/v2"
	"gitlab.com/nerzhul/nsmtpd/internal/common"
)

type Config struct {
	Port        uint16
	TLSKeyPath  string
	TLSCertPath string

	AuthMethod string

	LDAP common.LDAPConfig
	HTTP common.HTTPConfig

	Extensions struct {
		Compress bool
	}

	DynamicAliasing common.DynamicAliasingConfig

	Database    dbConfig

	Debug bool
}

var GConfig Config

const (
	IMAPDelimiter  = "."
	AuthMethodDB   = "database"
	AuthMethodLDAP = "ldap"
)

func (c *Config) loadConfigFromEnv() {
	err := envconfig.Process("", c)
	if err != nil {
		klog.Fatal(err.Error())
	}
}

func (c *Config) Load() {
	klog.InitFlags(flag.CommandLine)
	flag.Parse()

	c.Port = 1143

	c.AuthMethod = AuthMethodDB

	c.LDAP.URL = "ldap://localhost"

	// For relay auth test
	c.LDAP.UserPattern = "uid=%s,ou=people,dc=example,dc=org"

	// For email recv test
	c.LDAP.BindDN = "cn=read-only,ou=people,dc=example,dc=org"
	c.LDAP.BindPassword = "password"
	c.LDAP.BaseDN = "dc=example,dc=org"
	c.LDAP.DomainFilter = "(&(objectClass=domain)(dc=%s))"
	c.LDAP.DomainAttr = "dc"
	c.LDAP.AliasFilter = "(&(objectClass=nisMailAlias)(cn=%s))"
	c.LDAP.AliasAttr = "rfc822MailMember"
	c.LDAP.UserNameAttr = "uid"
	c.LDAP.UserFilter = "(&(objectClass=posixAccount)(uid=%s))"

	c.HTTP.Port = 8080
	c.HTTP.AuthUser = "nimapd"
	c.HTTP.AuthPassword = "password"
	c.HTTP.EncryptKey = ""

	c.Database.URL = "host=postgres dbname=nimapd user=nimapd password=nimapd"
	c.Database.MaxOpenConns = 10
	c.Database.MaxIdleConns = 5
	c.Database.MigrationSource = "file:///migrations"

	c.Extensions.Compress = true

	c.DynamicAliasing.Enable = false
	c.DynamicAliasing.Separator = "+"

	c.Debug = false

	c.loadConfigFromEnv()

	if c.AuthMethod != AuthMethodDB && c.AuthMethod != AuthMethodLDAP {
		klog.Fatalf("Invalid auth method configured: %s. Valid values are database, ldap.", c.AuthMethod)
	}

	GDatabase.Config = &c.Database
	klog.Info("Configuration loaded.")
}

func (c *Config) GetListeningAddr() string {
	return fmt.Sprintf(":%d", c.Port)
}
