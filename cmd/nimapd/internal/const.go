package internal

import (
	"github.com/emersion/go-imap"
	"gitlab.com/nerzhul/nsmtpd/pkg/nimapd/message"
)

const (
	MailboxInbox  = "INBOX"
	MailboxDrafts = "Drafts"
	MailboxSent   = "Sent"
	MailboxTrash  = "Trash"
)

type messageFilterCallback func(message *message.Message, seqset *imap.SeqSet) bool
