package internal

import (
	"fmt"

	"golang.org/x/crypto/bcrypt"
	"k8s.io/klog/v2"
)

type DBUserValidator struct{}

func NewDBUserValidator() UserValidator {
	return &DBUserValidator{}
}

func (u *DBUserValidator) Login(username string, password string) (string, error) {
	userID, err := GDatabase.GetUserID(username)
	if err != nil {
		klog.Errorf(err.Error())
		return "", fmt.Errorf("unable to login: server error")
	}

	if userID == "" {
		klog.Errorf("unable to login: username %s not found", username)
		return "", fmt.Errorf("unable to login: username or password incorrect")
	}

	hashedPassword, salt, err := GDatabase.GetUserSecrets(userID)
	saltedPwd := fmt.Sprintf("%s/%s", password, salt)

	err = bcrypt.CompareHashAndPassword(hashedPassword, []byte(saltedPwd))
	if err != nil {
		klog.Errorf("auth failed for user %s (password mismatch)", username)
		return "", fmt.Errorf("unable to login: username or password incorrect")
	}

	klog.Infof("user %s authed successfully", username)
	return userID, nil
}

func (u *DBUserValidator) Exists(username string) (bool, error) {
	found, err := u.dbExists(username)
	if err != nil {
		return false, fmt.Errorf("unable to find user: %s", err.Error())
	}

	if !found {
		return false, fmt.Errorf("user not found")
	}

	return true, nil
}

func (u *DBUserValidator) dbExists(username string) (bool, error) {
	userID, err := GDatabase.GetUserID(username)
	if err != nil {
		klog.Errorf(err.Error())
		return false, fmt.Errorf("server error")
	}

	if userID == "" {
		klog.Errorf("username %s not found in database", username)
		return false, nil
	}

	return true, nil
}
