package internal

import (
	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/backend"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"k8s.io/klog/v2"
	"time"
)

type Backend struct {
	backend.BackendUpdater
	userValidator    UserValidator
	counterLogin     prometheus.Counter
	counterLoginTime prometheus.Counter
	updateChannel    chan backend.Update
}

func (b *Backend) Login(connInfo *imap.ConnInfo, username, password string) (backend.User, error) {
	b.counterLogin.Inc()
	startTime := time.Now().Nanosecond()
	defer b.counterLoginTime.Add(float64(time.Now().Nanosecond()) - float64(startTime))

	klog.V(0).Infof("obj=backend type=login msg='Trying to authenticate user %s'", username)
	userID, err := b.userValidator.Login(username, password)
	if err != nil {
		klog.V(0).Infof("obj=backend type=login msg='User %s' failed to auth: %s", username, err)
		return nil, err
	}

	klog.V(0).Infof("obj=backend type=login msg='User %s' authed", username)
	return NewUser(userID, username, b.updateChannel), nil
}

func (b *Backend) Updates() <-chan backend.Update {
	return b.updateChannel
}

func NewBackend(uv UserValidator) *Backend {
	return &Backend{
		userValidator: uv,
		counterLogin: promauto.NewCounter(prometheus.CounterOpts{
			Name: "nimapd_backend_logins",
			Help: "Login command call count",
		}),
		counterLoginTime: promauto.NewCounter(prometheus.CounterOpts{
			Name: "nimapd_backend_login_total_time",
			Help: "Total time spent in login command",
		}),
		updateChannel: make(chan backend.Update, 1),
	}
}
